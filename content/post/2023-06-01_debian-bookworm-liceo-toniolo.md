---
title: "Il Liceo Toniolo è in testa."
subtitle: "E` stato il primo istituto a fare l'aggionamento a FUSS 12"
date: 2023-06-01T10:21:01+02:00
draft: false
---

<img src="/img/fuss-12-toniolo.jpg" alt="Aula informatica con FUSS 12 al Liceo Toniolo">

Il **Liceo Scientifico "Toniolo"** di Bolzano, scuola pilota della Rete FUSS, per prima ha effettuato in data 1 giugno 2023 l'aggiornamento a FUSS 12 della sua dotazione informatica sia lato server che sui client.

<!--more-->

Con 10 giorni d'anticipo dall'uscita ufficiale di [**Debian 12 "bookworm"**](https://www.debian.org/releases/testing/amd64/release-notes/index.it.html) su cui FUSS 12 si basa, la scuola ha potuto mantenere i propri standard di sicurezza grazie ai software di sistema ed applicativi costantemente aggiornati. La creazione di FUSS 12 e la sua installazione anticipata è stata possibile grazie al fatto che la distribuzione GNU/Linux [**Debian**](https://www.debian.org/) subisce dal 12 gennaio 2023 diversi stadi di [*freeze* (congelamento)](https://release.debian.org/testing/freeze_policy.html) fino all'ultimo (*full freeze*) iniziato il 24 maggio e che di norma avviene 2 settimane prima del rilascio.

Il Liceo Toniolo è divenuto nel dicembre 2019 un nuovo [**Centro di Competenza sulla Sostenibilità Digitale**](https://fuss.bz.it/post/2019-12-12_fusslab-2/) nella Rete di scuole FUSS. Il suo laboratorio, aperto alle necessità didattiche dell’Istituto è nel contempo aperto al territorio in modalità centro di ricerca e sviluppo FUSS e punto di riferimento per la formazione informatica di docenti e dirigenti nonché delle aziende locali interessate a condividere la visione di digitalizzazione sostenibile iniziata nel 2005 da FUSS.

A titolo d'esempio, nel corso dell'a.s. 2022-23 l'aula è stata impiegata in più riprese per la formazione docenti, in particolare per proporre attività legate alla promozione del pensiero computazionale attraverso la computazione fisica.

<img src="/img/fuss-12-wallpaper.png" alt="FUSS 12 wallpaper" width="600">
