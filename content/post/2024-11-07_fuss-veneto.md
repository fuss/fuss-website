---
title: "Buone pratiche nelle scuole del Veneto"
subtitle: "Il Progetto FUSS partecipa ai gruppi di lavoro regionali sull’educazione alla legalità"
date: 2024-11-07T22:34:01+02:00
draft: false
---

Il Progetto FUSS, nel corso dell'a.s. 2023-2024 ha aderito all’iniziativa **Gruppi di lavoro regionali sull’Educazione alla Legalità** posta in essere dall’**USR per il Veneto**, in collaborazione con gli **UU.AA.TT.**
Al seguente link la notizia relativa alla pubblicazione del volume digitale [**Educazione alla Legalità: l’esperienza di una progettualità condivisa**](https://istruzioneveneto.gov.it/20241104_32719/).

<!--more-->

Le attività formative di questo tavolo di lavoro, composto da un centinaio di docenti delle scuole di ogni ordine e grado della **Regione Veneto**, sono state articolate in quattro incontri formativi/informativi e online grazie al coordinamento delle **referenti Legalità, Politiche Giovanili e Partecipazione** degli Uffici Ambiti Territoriali di Verona – **Roberta Spallone** - e di Vicenza – **Emanuela Ropele**.

Il Sottogruppo 5 **Piattaforme open-source** è composto da 4 docenti di diversi ordini di scuola e di varie provenienze territoriali ed un ispettore tecnico:

- **Gianluca Maestra** - Docente di scuola secondaria di secondo grado di
  Padova
- **Monica Marton** - docente e collaboratrice del dirigente scolastico
  presso l’Istituto Comprensivo 3 di Vicenza
- **Eleonora Leta** - docente e collaboratrice del irigente scolastico
  dell’Istituto Comprensivo 8 di Verona
- **Nicola Vicentini**, docente presso l’Istituto Comprensivo 5 di Padova
- **Paolo Dongilli** - Coordinatore del Progetto FUSS ed ispettore
  presso l'Intendenza Scolastica Italiana, Provincia Autonoma di
  Bolzano.

Il percorso si è sviluppato in una serie di incontri che hanno visto docenti, tecnici ed esperti confrontarsi in merito alla natura del software libero, al suo impiego, ad esempi virtuosi di utilizzo, sino a domandarsi come "esportare" l’esperienza di FUSS di Bolzano, replicandola in Veneto, considerando le particolarità della scuola dell’autonomia in un ambito regionale non autonomo.
Alla realizzazione di questo progetto ha collaborato anche il seguente personale esterno:

- **Albano Battistella** - progetto **Zorin OS**
- **Marco Marinello** - ditta **Qnets**
- **Mauro Biasutti** - ditta **Continuity**
- **Fabio Lovato** - Presidente di **ILS Este**.

Viene messa a disposizione, tutta la [**documentazione**](https://fuss.bz.it/fuss-veneto) prodotta dal **Sottogruppo 5** al link [**https://fuss.bz.it/fuss-veneto**](https://fuss.bz.it/fuss-veneto).

Gli istituti di Vicenza, Verona e Padova sopra indicati si aggiungono all’[**Istituto Professionale “E. Cornaro” per i Servizi di Enogastronomia e Ospitalità Alberghiera di Jesolo**](https://fuss.bz.it/post/2018-07-12_esperienza-ipseoa-jesolo/), che grazie ai suoi 16 anni di esperienza con FUSS e Software Libero, rappresenta un punto di riferimento in Veneto in termini di sostenibilità e sovranità digitale; il merito va alla lungimiranza della propria dirigenza d'istituto ed al costante impegno del responsabile informatico **Luca Marcolongo**. 

**L'Ufficio Scolastico Regionale per il Veneto - Ufficio II** auspica che il lavoro svolto dai docenti che hanno elaborato le progettazioni, presenti nella [**pubblicazione**](https://istruzioneveneto.gov.it/wp-content/uploads/2024/11/Educazione-alla-Legalita_lesperienza-di-una-progettualita-condivisa-1.pdf), sia uno stimolo per sperimentare, nei propri Istituti e con i propri alunni e studenti, le attività/i progetti così da renderli Buone Pratiche da condividere con l’USR, partecipando al monitoraggio di **Educazione alla Legalità** previsto per quest’anno scolastico, che ne consentirà la pubblicazione nel **Catalogo regionale di Educazione alla Legalità**.




