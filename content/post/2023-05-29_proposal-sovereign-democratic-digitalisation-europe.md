---
title: "Event at the European Parliament" 
subtitle: "Proposal for a Sovereign and Democratic Digitalisation of Europe"
date: 2023-05-29T12:23:00+02:00
draft: false
---

[<img src="/img/proposal-eu.jpg" alt="Proposal for a Sovereign and Democratic Digitalisation of Europe">](https://xnet-x.net/en/event-european-parliament-democratic-digitalisation-eu/)

*Framework for a coalition’s action*
#DemocraticDigitalisation

**Wednesday May 31st**, from **13:00 - 15:00PM** **European Parliament (Brussels)**, **Room**: Spinelli 5E1

Starting from the report [“**Proposal for a Sovereign and Democratic Digitalisation of Europe**“](https://democratic-digitalisation.xnet-x.net/), the event wants to propose a framework to promote concrete actions and policies to redefine the foundations of EU digital transformation towards a solid digital rights-centred approach.

<!--more-->

**Host**

  * MEP Stelios Kouloglou

**Organiser**

  * [Xnet](https://xnet-x.net/en/) – Digital Rights and Democracy
 
**Schedule**

  * Introduction by [Stelios Kouloglou](https://www.europarl.europa.eu/meps/en/130833/STELIOS_KOULOGLOU/home), The Left Group in the European Parliament, and [Patrick Breyer](https://www.europarl.europa.eu/meps/en/197431/PATRICK_BREYER/home), Group of the Greens/European Free Alliance in the European Parliament.

  * [Simona Levi](https://en.wikipedia.org/wiki/Simona_Levi), Director of Xnet, Digital Rights and Democracy and lead author of “[Proposal for a Sovereign and Democratic Digitalisation of Europe](https://democratic-digitalisation.xnet-x.net/)” – report petitioned by the former President of the European Parliament David Sassoli – Publication Office of the European Union.

  * Bram Vranken, Researcher at [Corporate Europe Observatory](https://corporateeurope.org/en) presenting “[The lobby network: Big Tech’s web of influence in the EU](https://corporateeurope.org/en/2021/08/lobby-network-big-techs-web-influence-eu)” and “[The lobbying ghost in the machine](https://corporateeurope.org/en/2021/08/lobby-network-big-techs-web-influence-eu)“.

  * [Pina Picierno](https://www.europarl.europa.eu/meps/en/124846/PINA_PICIERNO/home), Vice President of the European Parliament, Group of the Progressive Alliance of Socialists and Democrats: Sassoli’s legacy on the field of digital rights.

  * Floor to authorities and organisations in the first row, such as [European Parents’ Association](https://europarents.eu/) and Transnational Institute author of the Report “[Digital Power – State of Power 2023](https://www.tni.org/en/publication/stateofpower2023)“.

  * Press, Q&A and debate with MEPs, authorities and participants.

**For further details**: [https://xnet-x.net/en/event-european-parliament-democratic-digitalisation-eu/](https://xnet-x.net/en/event-european-parliament-democratic-digitalisation-eu/)

**Source**: [https://xnet-x.net](https://xnet-x.net) - **License**: CC BY-SA 3.0
