---
title: "Crea tu lo sfondo!"
subtitle: "Al via un'originale iniziativa artistica per le scuole"
date: 2022-09-21T12:45:11+02:00
draft: false
---

![Sfondo FUSS settembre](/img/backdrop_2022_09.png)

Da febbraio del 2022 i computer collegati alla rete didattica delle scuole mostrano nella maschera di login uno sfondo avente come tema una lavagna di ardesia con un'immagine che varia di mese in mese. 

<!--more-->

Inoltre, in occasione di alcune ricorrenze particolari (anniversari di nascita/morte artisti, scienziati, letterati e altro), l’immagine del mese viene temporaneamente cambiata con un'immagine che ricorda la ricorrenza della giornata.

Quest’anno scolastico l'idea è di pubblicare disegni realizzati da alunni che frequentano le scuole della nostra provincia.

Le scuole/docenti che desiderano partecipare all’iniziativa possono comunicare ad [Andrea.Bonani@scuola.alto-adige.it](mailto:Andrea.Bonani@scuola.alto-adige.it) la loro adesione. Successivamente i docenti saranno contattati per maggiori dettagli operativi.

I disegni inviati saranno pubblicati con la licenza [Creative Commons BY-NC](https://creativecommons.org/licenses/by-nc/4.0/) sulla [presente pagina web](https://fuss.bz.it/crea-tu-lo-sfondo) ([https://fuss.bz.it/crea-tu-lo-sfondo](https://fuss.bz.it/crea-tu-lo-sfondo)).
Per ulteriori informazioni scrivere ad [Andrea.Bonani@scuola.alto-adige.it](mailto:Andrea.Bonani@scuola.alto-adige.it).

## Sfondi realizzati

[<img src="/img/sfondi/thumb-2022_10.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/monthly/2022_10.png)
---
[<img src="/img/sfondi/thumb-2022_10_05.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_10_05.png)
---
[<img src="/img/sfondi/thumb-2022_10_12.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_10_12.png)
---
[<img src="/img/sfondi/thumb-2022_10_21.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_10_21.png)
---
[<img src="/img/sfondi/thumb-2022_10_25.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_10_25.png)
---
[<img src="/img/sfondi/thumb-2022_10_27.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_10_27.png)
---
[<img src="/img/sfondi/thumb-2022_11_01-15.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_11_01-15.png)
---
[<img src="/img/sfondi/thumb-2022_11_11.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_11_11.png)
---
[<img src="/img/sfondi/thumb-2022_11_16.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_11_16.png)
---
[<img src="/img/sfondi/thumb-2022_11_16-30.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_11_16-30.png)
---
[<img src="/img/sfondi/thumb-2022_11_24.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_11_24.png)
---
[<img src="/img/sfondi/thumb-2022_12.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/monthly/2022_12.png)
---
[<img src="/img/sfondi/thumb-2022_12_07.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_12_07.png)
---
[<img src="/img/sfondi/thumb-2022_12_15.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_12_15.png)
---
[<img src="/img/sfondi/thumb-2022_12_19.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_12_19.png)
---
[<img src="/img/sfondi/thumb-2022_12_21.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2022_12_21.png)
---
[<img src="/img/sfondi/thumb-2023_01.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/monthly/2023_01.png)
---
[<img src="/img/sfondi/thumb-2023_01_11.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_01_11.png)
---
[<img src="/img/sfondi/thumb-2023_01_17.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_01_17.png)
---
[<img src="/img/sfondi/thumb-2023_01_19.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_01_19.png)
---
[<img src="/img/sfondi/thumb-2023_01_23.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_01_23.png)
---
[<img src="/img/sfondi/thumb-2023_02_01.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_01.png)
---
[<img src="/img/sfondi/thumb-2023_02_01-07.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_01-07.png)
---
[<img src="/img/sfondi/thumb-2023_02_08.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_08.png)
---
[<img src="/img/sfondi/thumb-2023_02_08-14.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_08-14.png)
---
[<img src="/img/sfondi/thumb-2023_02_15.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_15.png)
---
[<img src="/img/sfondi/thumb-2023_02_15-21.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_15-21.png)
---
[<img src="/img/sfondi/thumb-2023_02_22-28.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_22-28.png)
---
[<img src="/img/sfondi/thumb-2023_02_28.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_02_28.png)
---
[<img src="/img/sfondi/thumb-2023_03.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/monthly/2023_03.png)
---
[<img src="/img/sfondi/thumb-2023_03_02.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_03_02.png)
---
[<img src="/img/sfondi/thumb-2023_03_13.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_03_13.png)
---
[<img src="/img/sfondi/thumb-2023_03_20.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_03_20.png)
---
[<img src="/img/sfondi/thumb-2023_03_27.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_03_27.png)
---
[<img src="/img/sfondi/thumb-2023_03_30.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_03_30.png)
---
[<img src="/img/sfondi/thumb-2023_04.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/monthly/2023_04.png)
---
[<img src="/img/sfondi/thumb-2023_04_03-05.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_04_03-05.png)
---
[<img src="/img/sfondi/thumb-2023_04_12.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_04_12.png)
---
[<img src="/img/sfondi/thumb-2023_04_20.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_04_20.png)
---
[<img src="/img/sfondi/thumb-2023_04_21.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_04_21.png)
---
[<img src="/img/sfondi/thumb-2023_05_11.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_05_11.png)
---
[<img src="/img/sfondi/thumb-2023_05_16.png">](https://gitlab.fuss.bz.it/fuss/fuss-artwork/-/blob/master/fuss-artwork/wallpapers/daily/2023_05_16.png)
---
