---
title: "Guida a Minetest per docenti ed alunni" 
subtitle: "Il lavoro di due studenti dell'IISS 'G. Galilei' al Laboratorio FUSS di Sostenibilità Digitale"
date: 2022-10-29T21:16:12+02:00
draft: false
---

<img src="/img/minetest-logo.png" alt="Minetest Logo" width="200">

Si è concluso il 28 ottobre il tirocinio formativo (PCTO) di due
studenti al Laboratorio FUSS di Sostenibilità Digitale presso
l'Intendenza Scolastica Italiana. **Daniel Dal Bosco** e **Matteo
Azzolini Poggioli**, iscritti al 5° anno presso l'IISS "G. Galilei" di
Bolzano si sono dovuti confrontatare con una nuova proposta didattica
di interesse per le scuole di ogni ordine e grado.

<!--more-->

![Foto tirocinio PCTO su Minetest](/img/foto-pcto-minetest.jpg)
Daniel Dal Bosco e Matteo Azzolini Poggioli nel Laboratorio FUSS.

Daniel e Matteo hanno lavorato con
[**Minetest**](https://www.minetest.net/), un
applicativo/videogioco *sandbox* basato su
[*voxel*](https://en.wikipedia.org/wiki/Voxel). Come descrivono nella
[guida](/material/minetest/guida-minetest.pdf) da loro redatta,
per impostazione predefinita, il gioco si svolge in prima persona, ma
i giocatori possono scegliere la prospettiva in terza persona. Il
mondo di gioco è composto da *voxel*: oggetti 3D, molti dei quali
semplici cubi, comunemente chiamati "nodi" all'interno di una griglia
regolare. Il concetto di "voxel" di Minetest è simile a quello dei
blocchi da costruzione (come i blocchi LEGO). Nel mondo interattivo si
possono usare i "blocchi" per costruire oggetti e attrezzi di uso
quotidiano.

![Minetest main screen](/img/minetest-main-screen.png)
Schermata principale di Minetest. Fonte: Wikipedia

Si possono costruire case, fattorie, città, strumenti e molte altre
cose incredibilmente creative a partire dai "blocchi" materiali. Ad
esempio, il minerale metallico viene estratto, intagliato in lingotti,
utilizzato per creare picconi o altri oggetti di strumenti diversi,
che vengono poi utilizzati per costruire edifici o qualsiasi altra
struttura che si possa immaginare. E a differenza dei costosi
mattoncini LEGO, si può camminare intorno alle proprie creazioni e
sperimentare il nuovo mondo che si è creato. Ad esempio, si possono
costruire e utilizzare scale, si può incendiare la sabbia in blocchi
di vetro e trasformarli in finestre attraverso le quali si può
osservare il sorgere del sole quadrato nel mondo di Minetest dalla
propria veranda.

![Minetest farm](/img/minetest-farm.jpg)
Fonte: Wikipedia

Minetest è disponibile per GNU/Linux, FreeBSD, OpenBSD, DragonFly
BSD, Windows, macOS ed Android. Il grande vantaggio di Minetest
rispetto al noto Minecraft è quello di essere software libero, ciò
significa che gli utenti hanno la libertà di eseguire, copiare,
studiare, distribuire, migliorare e modificare il software. Oltre ad
essere libero è anche gratuito, funziona bene su hardware datato, non
dipende da nessuna multinazionale o entità commerciale (pertanto è
indipendente) e non è vincolato da alcuna
[EULA](https://it.wikipedia.org/wiki/EULA) (End User License
Agreement) che ne limiti l'uso e pertanto la crescita.

Nella sua versione base il gioco non ha un obiettivo predefinito e
lascia dunque molto spazio alla fantasia del giocatore nel creare
quello che più desidera.  Le mappe sono generate automaticamente dal
software e vi sono più modalità di generazione di queste ultime
permettendo all'utente di scegliere quella che più desidera durante la
configurazione di un nuovo mondo.

I notevoli gradi di libertà offerti da Minetest lo rendono
particolarmente adatto a diverse attività didattiche
interdisciplinari, richiamate da diversi siti specializzati quali:

- Minetest for Education (https://www.minetest.net/education/)
- MinetestEDU (https://wiki.minetest.net/MinetestEDU/)
- Minetest Education Edition: https://github.com/edu-minetest/minetest
- Educational Mods: https://wiki.minetest.net/Mods:List_of_educational_mods
- Framinetest Edu: https://framinetest.org/it/

Minetest viene utilizzato in ambienti educativi per insegnare materie
come la matematica, la programmazione e le scienze della terra. A
titolo d'esempio seguono alcune esperienze:

- Nel 2017 in Francia, Minetest è stato utilizzato per insegnare il
  calcolo e la trigonometria.
- All'Università Federale di Santa Catarina in Brasile, Minetest è
  stato utilizzato per insegnare la programmazione in una variante
  chiamata MineScratch.
- Nel 2018, per il Laboratorio di Educazione e Apprendistato (EDA)
  dell'Università Paris Descartes, Minetest è stato utilizzato per
  insegnare le scienze della vita e della terra agli studenti del
  sesto anno che non hanno potuto osservare di persona alcuni
  fenomeni, ma hanno potuto sperimentarli nel mondo virtuale di
  Minetest.

Questo e molto altro potete trovare nell'ottima [Guida a Minetest](/material/minetest/guida-minetest.pdf) scritta da Matteo e Daniel a conclusione del loro lavoro.

[<img src="/img/minetest-guide-screenshot.png" alt="Screenshot Guida a Minetest" width="400" border="1">](/material/minetest/guida-minetest.pdf)
