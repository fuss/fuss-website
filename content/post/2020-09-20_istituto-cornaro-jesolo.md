---
title: "FUSS 10 al Cornaro di Jesolo" 
subtitle: "Aggiornati ed operativi i sistemi informatici dell'istituto"
date: 2020-09-20T11:14:03+02:00
draft: false
---

![Foto al Cornaro di Jesolo](/img/istituto-cornaro-jesolo-2020.jpg)
**Luca Marcolongo** (responsabile informatico), **Raffaele Trotta** (vicepreside) ed **Emilio Martin** (docente)

All'inizio dell'anno scolastico tutto deve funzionare a puntino, anche e soprattutto le attrezzature informatiche in un periodo in cui "didattica a distanza" (o DaD) e "didattica digitale integrata" sono entrate nel vocabolario della scuola.

[L'IPSOEA "E. Cornaro" di Jesolo](https://fuss.bz.it/post/2018-07-12_esperienza-ipseoa-jesolo/) che conta circa 800 studenti, dal 2008 ha preferito scegliere una via sostenibile alla digitalizzazione adottando FUSS.

<!--more-->

Nel corso del precedente anno scolastico Luca Marcolongo, responsabile informatico dell'istituto, ha pianificato con attenzione tutti i dettagli. Durante l'estate ha provveduto ad aggiornare e configurare prima il server alla versione 10 di FUSS (basata su [Debian 10 "buster"](https://debian.org)) garantendo così a tutti i PC della scuola un accesso sicuro ai dati in ottemperanza alle Misure minime di sicurezza previste dall'Agenzia per l'Italia Digitale (AgID) per le PA. 

Il server FUSS gestisce più di 200 PC di cui 40 notebook FUSS 10 utilizzati nelle classi; funge anche da domain controller per 165 PC Windows presenti nell'istituto e permette di gestire utenti, file, condivisioni ed accesso ad internet. 
Ciascuna classe è dotata di una LIM pilotata pure da FUSS grazie all'applicativo [OpenBoard](https://openboard.ch/), potente strumento open source presente nella distribuzione per utilizzare al meglio le lavagne multimediali ed usato da numerose scuole ed università in tutto il mondo.

Le nostre più vive congratulazioni all'Istituto "E. Cornaro" per l'eccellente lavoro svolto per una ripartenza digitale in sicurezza del nuovo anno scolastico contribuendo al riuso del *valore pubblico* creato dal progetto FUSS per le scuole e la collettività.


