---
title: "Debian 12 'bookworm' and FUSS 12"
subtitle: "On June 10th the 12th release of Debian and FUSS will be celebrated at SPAZIO77"
date: 2023-06-03T10:21:01+02:00
draft: false
---

Preparations for celebrating the planned release on **Saturday June 10th** of **Debian 12 "bookworm"** are in full swing. Currently Debian Bookworm Release Parties are planned in 3 continents: Bolivia and Brazil; Belgium, Germany, Italy, Netherlands and Portugal; and in Iran. See [https://wiki.debian.org/ReleasePartyBookworm](https://wiki.debian.org/ReleasePartyBookworm).

<img src="/img/debian-12-banner.png" alt="Debian 12 banner">

<!--more-->

In South Tyrol [**SPAZIO77**](https://www.openstreetmap.org/search?whereami=1&query=46.49189%2C11.33389#map=19/46.49189/11.33389&layers=N) (Via Dalmazia - Dalmatienstraße 77, Bozen-Bolzano) will host the [local event](https://wiki.debian.org/ReleasePartyBookworm#ReleasePartyBookworm.2FItaly.2FBozenBolzano.Italy:_Bozen-Bolzano) during which the release of **FUSS 12** will also be presented and celebrated by the FUSS Project, the [LUGBZ](https://lugbz.org) and [SchoolSwap](https://schoolswap.bz.it). Mark the date: **Saturday, June 10th from 18:00 to 20:00**.

Further info can be found on the [Debian Wiki page](https://wiki.debian.org/ReleasePartyBookworm#ReleasePartyBookworm.2FItaly.2FBozenBolzano.Italy:_Bozen-Bolzano) dedicated to the release day.

This year another big day will be celebrated: on [**August 16th 2023**](https://wiki.debian.org/DebianDay/2023) the Debian operating system will turn 30! We won't miss this opportunity to meet again! Stay tuned!

<img src="/img/debian-12-wallpaper.png" alt="Debian 12 wallpaper" width="600">

<img src="/img/fuss-12-wallpaper.png" alt="FUSS 12 wallpaper" width="600">
