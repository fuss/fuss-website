---
title: "Il valore dell’open source per un'Europa digitale indipendente e competitiva" 
subtitle: "Un'ulteriore conferma per il Progetto FUSS dalla Commissione Europea e dal MITD"
date: 2021-09-27T21:12:17+02:00
draft: false
---

Il Ministero per l'innovazione tecnologica e la transizione digitale cita il recente [**studio della Commissione Europea**](https://innovazione.gov.it/notizie/articoli/il-valore-dell-open-source-per-un-europa-digitale-indipendente-e-competitiva/) che fa luce sulle potenzialità economiche dell'open source e raccomanda agli Stati Membri investimenti dedicati.

Grazie a FUSS le scuole in Provincia di Bolzano da 16 anni sono sul binario giusto e rappresentano un faro a livello nazionale ed europeo del quale dovremmo andare fieri.

<!--more-->

Dal sito del Ministero:

> La Commissione Europea promuove le politiche pubbliche sull'open source e punta sulle iniziative strategiche, come Developers Italia, per favorire ulteriormente lo sviluppo industriale di questo settore e la diffusione di competenze nel settore pubblico.

> L'analisi nasce da uno studio recentemente pubblicato dall’unità DG CONNECT della Commissione Europea che mette in luce l’impatto del software e hardware open source sull’economia dell’Unione, con particolare riferimento a innovazione, competitività, indipendenza tecnologica e creazione di posti di lavoro.*

> Secondo lo studio, nel 2018 le aziende situate nell’area EU hanno investito circa 1 miliardo di euro in software open source (OSS), con un impatto sull’economia dell’eurozona stimato tra i 65 e 95 miliardi di euro. Sempre secondo lo studio un aumento del 10% sugli investimenti in OSS potrebbe contribuire alla nascita di oltre 600 nuove start-up in EU, generando un aumento di PIL di 100 miliardi di euro.

> In aggiunta, lo studio evidenza come usare esclusivamente software open source invece di software proprietario possa favorire l’indipendenza del settore pubblico nell’Unione, riducendo i costi e limitando il fenomeno del lock-in (la dipendenza dai fornitori). In sostanza, l’open source è la chiave per un'Unione Europea più autonoma e sovrana dal punto di vista tecnologico. 

**Riferimenti:** 

1. Il valore dell’open source per un’Europa digitale indipendente e competitiva, [https://innovazione.gov.it/notizie/articoli/il-valore-dell-open-source-per-un-europa-digitale-indipendente-e-competitiva/](https://innovazione.gov.it/notizie/articoli/il-valore-dell-open-source-per-un-europa-digitale-indipendente-e-competitiva/)
2. Study about the impact of open source software and hardware on technological independence, competitiveness and innovation in the EU economy, [https://digital-strategy.ec.europa.eu/en/library/study-about-impact-open-source-software-and-hardware-technological-independence-competitiveness-and](https://digital-strategy.ec.europa.eu/en/library/study-about-impact-open-source-software-and-hardware-technological-independence-competitiveness-and)
