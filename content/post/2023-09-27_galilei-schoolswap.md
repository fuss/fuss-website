---
title: "Economia circolare a scuola"
subtitle: "L'IISS Galilei contribuisce a pieno titolo al progetto SchoolSwap"
date: 2023-09-27T14:44:00+02:00
draft: false
---

![PC con FUSS 12 ricondizionato dall'IISS Galilei](/img/iiss-galilei-schoolswap-1.jpg)

Sono già 20 i PC che l'Istituto Superiore "G. Galilei" ha fornito ai
propri studenti.  Ecco un esempio concreto di **economia circolare** e
**sostenibilità digitale** attuato dall'istituto bolzanino il quale,
anziché conferire in discarica PC ancora funzionanti - come molte
realtà pubbliche e private oggigiorno purtroppo fanno - ha preferito
pulirli e ricondizionarli per poi donarli ai propri studenti.

<!--more-->

L'impegno di **Fabio Ferraris** e **Matteo Gottardi**, referenti
informatici del Galilei, di concerto con la dirigenza scolastica, ha
permesso di dar vita a questo progetto che assume un significato
particolarmente importante soprattutto dal punto di vista didattico,
in quanto non termina con la semplice consegna di un computer ad una
studentessa o ad uno studente. Ancor più permette di comunicare alla
comunità scolastica e di rimando alle famiglie ed ai cittadini le
positive implicazioni legate al riuso dei beni (in questo caso
attrezzature informatiche), alla necessaria riparabilità dei
dispositivi elettronici (v. [*right to repair
legislation*](https://www.europarl.europa.eu/news/en/headlines/society/20220331STO26410/why-is-the-eu-s-right-to-repair-legislation-important))
ed all'enorme ruolo ricoperto da GNU/Linux e dal Software Libero nel
permettere di sfruttare al meglio le potenzialità di un dispositivo
grazie alla loro modificabilità ed adattablità all'hardware usato. Sui
PC, infatti, è stata installata l'ultima versione di FUSS (basata su
Debian 12 "bookworm") uscita dal laboratorio FUSS a inizio giugno
completa di circa 200 software didattici per ogni ordine e grado di
scuola.

<img src="/img/iiss-galilei-schoolswap-2.jpg" alt="Fabio Ferraris - IISS G. Galilei" width="500">
<center>Fabio Ferraris - Referente tecnico dell'IISS Galilei</center>

Questo lodevole progetto dell'IISS Galilei aderisce all'iniziativa
locale
[**SchoolSwap**](https://fuss.bz.it/post/2021-05-26_schoolswap/) che a
partire da febbraio 2020, grazie ai volontari di
[**LUGBZ**](https://lugbz.org) e [**ADA** (Associazione per i Diritti
degli Anziani)](https://www.adanazionale.it/) ed alla collaborazione
di [**FUSS**](https://fuss.bz.it), [**Südtiroler -
Vinzenzgemeinschaft**](https://www.vinzenzgemeinschaft.it/)
e [**Jugenddienst Bozen**](https://www.jugenddienst.it), ha permesso
di consegnare **più di 750 PC** a studentesse e studenti delle scuole della
nostra provincia.
