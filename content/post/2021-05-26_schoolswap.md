---
title: "SchoolSwap" 
subtitle: "Un PC sostenibile per studiare e lavorare - Ein nachhaltiger PC zum Lernen und Arbeiten"
date: 2021-05-26T09:48:00+02:00
draft: false
---

[![Fotogramma e link al video di SchoolSwap](/img/schoolswap-small.png)](https://schoolswap.bz.it/wp/wp-content/uploads/2021/04/Schcoolswap_ITA_HQ.mp4)

Per agevolare la didattica a distanza durante il passato periodo di lockdown, l'associazione [LugBZ](https://lugbz.org) (Linux User Group Bolzano-Bozen-Bulsan) assieme ad [ADA (Associazione per i Diritti degli Anziani)](https://www.adanazionale.it/) hanno dato vita al Progetto [**SchoolSwap**](https://schoolswap.bz.it) un'iniziativa di sostegno a studenti e famiglie che ha riscosso un notevole successo: PC e notebook donati da privati, imprese ed enti pubblici sono stati aggiornati con il sistema *Debian GNU/Linux "FUSS"* e tutti i programmi open source usati nelle scuole.

<!--more-->

Il progetto vuole far comprendere le potenzialità del software libero / open source (FOSS) nell'ambito sociale e pubblico mettendo in grado la comunità di usufruire di servizi senza costi ulteriori legati a licenze o brevetti. Creare buone pratiche dove l'impiego di software libero permetta di diminuire il "digital divide" delle persone anziane o di categorie svantaggiate come famiglie in difficoltà o dei migranti.

Da Febbraio 2020 i volontari di SchoolSwap sono riusciti a consegnare oltre 300 computer a famiglie e studentesse/studenti che ne hanno fatto richiesta. Sui PC consegnati vengono preinstallati tutti i software didattici utilizzati nelle scuole e quanto serve per la didattica digitale integrata (DDI).

Il progetto SchoolSwap ha inoltre permesso di rafforzare la collaborazione tra diverse associazioni e progetti locali; oltra a LugBZ e ADA sono coivolti:

- [FUSS -  https://fuss.bz.it/](https://fuss.bz.it)
- [Südtiroler - Vinzenzgemeinschaft https://www.vinzenzgemeinschaft.it](https://www.vinzenzgemeinschaft.it/)
- [Jugenddienst Bozen - https://www.jugenddienst.it](https://www.jugenddienst.it)

Per ulteriori informazioni consultare il sito [**https://schoolswap.bz.it**](https://schoolswap.bz.it)

[![SchoolSwap logo](/img/schoolswap-logo-small.png)](https://schoolswap.bz.it)

