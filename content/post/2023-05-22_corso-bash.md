---
title: "Corso BASH"
subtitle: "Disponibile la traduzione in lingua italiana della guida di Dashamir Hoxha"
date: 2023-05-22T13:15:07+02:00
draft: false
---

<img src="/img/bash.png" alt="BASH Logo" width="600">

E` disponibile sul sito FUSS [**https://fuss.bz.it/bash**](https://fuss.bz.it/bash) a cura del prof. Claudio Cavalli la traduzione e revisione in lingua italiana del corso sull'interfaccia a linea di comando pubblicato da [Dashamir Hoxha](https://gitlab.com/dashohoxha).

Tra le diverse *shell* esistenti, il corso usa la **Bash** (acronimo per **B**ourne **A**gain **SH**ell), sviluppata nell'ambito del progetto GNU come alternativa libera di Bourne shell. Si tratta di un interprete di comandi molto potente che permette all'utente di comunicare col sistema operativo attraverso una serie di funzioni predefinite, o di eseguire programmi e script. 

<!--more-->
