---
title: "Nuove immagini ISO di FUSS client"
subtitle: "E' disponibile le nuova versione 9.5.1"
date: 2018-08-19T07:30:23+02:00
draft: false
---

A seguito dell'aggiornamento di Debian 9 alla versione 9.5, il Nucleo FUSS ha provveduto ad aggiornare di conseguenza i file immagine (ISO) di FUSS Client disponibili nella sezione [Download](https://www.fuss.bz.it/page/download/) del sito.

<!--more-->

![Chiavetta FUSS](/img/fuss-usb-key-500.jpg)

Per copiare un'immagine su chiavetta USB, seguire le istruzioni contenute nel paragrafo [Preparazione chiavetta USB](https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#preparazione-chiavetta-usb) del Manuale Tecnico. 

