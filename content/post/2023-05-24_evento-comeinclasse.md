---
title: "Come scrollarsi di dosso le Big Tech ed il loro cloud" 
subtitle: "Presentata alle scuole della Campania una soluzione FOSS nazionale completa conforme a CAD e GDPR"
date: 2023-05-24T22:52:14+02:00
draft: false
---

<img src="/img/unlock.jpg" alt="Unlock">

Sono ormai molti i DPO (Data Protection Officer) e le scuole da loro seguite sul territorio nazionale che hanno ravvisato la necessità di dismettere con urgenza le soluzioni proprietarie fintamente "gratuite" fornite dalle Big Tech statunitensi non conformi al GDPR. Prodotti che in apparenza si presentano come soluzioni per la didattica ma che di fatto negli ultimi anni hanno fatto il *full-load* dei dati di studenti e docenti alimentando esclusivamente quegli interessi privati che **Shoshana Zuboff**, docente presso la Harvard Business School, descrive con un'analisi approfondita nel suo testo [*The Age of Surveillance Capitalism*](https://en.wikipedia.org/wiki/The_Age_of_Surveillance_Capitalism).

Da queste premesse, dopo l'**Emilia Romagna**, il **Piemonte** e la **Sicilia**, a stretto giro anche le scuole della **Campania** hanno chiesto di avere la panoramica di una soluzione cloud nazionale libera per la didattica digitale integrata. Il **24 maggio 2023** si è tenuto un incontro on-line di due ore circa che ha visto la partecipazione di più di 70 tra dirigenti e docenti alla presenza del DPO dott. **Luigi Belardo** (Safe & Cert), dell'ing. **Gianni Frattini** e di **Giorgio Favaro**  (CEO Continuity) il quale ha mostrato con il proprio collaboratore **Mauro Biasutti** i dettagli della suddetta soluzione. La [**registrazione completa dell'incontro**](https://bbb9.comeinclasse.it/playback/presentation/2.3/d6378bdf77424df52b646e232163b6f34dcea567-1684934146895) è [**pubblicamente disponibile online**](https://bbb9.comeinclasse.it/playback/presentation/2.3/d6378bdf77424df52b646e232163b6f34dcea567-1684934146895).

<!--more-->
