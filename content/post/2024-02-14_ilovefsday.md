---
title: "I ♥ Free Software Day 2024"
subtitle: "South Tyrol celebrates the day dedicated to Free Software"
date: 2024-02-13T19:20:05+02:00
draft: false
---

[<img src="/img/i-love-free-software-day.jpg" alt="I Love Free Software Day Card">](https://fsfe.org/activities/ilovefs/index.en.html)

We often underestimate the power of a simple "Thank you". Free
Software contributors do important work for our society and they
deserve to be recognised. “[**I Love Free Software
Day**](https://fsfe.org/activities/ilovefs/index.en.html)”, celebrated
on **14 February**, is the perfect opportunity for you to express your
special gratitude and show your appreciation for Free Software. The
**LUGBZ**, the **FUSS Project** and **ERESiA** will host the [**Event in
South Tyrol**](https://lugbz.org/free-software-wednesdays/).

<!--more-->

Imagine a Free Software community that is welcoming and appreciates
its contrbutors. This community is motivating and friendly in the
interaction with each other. Imagine if somebody new or old wants to
participate and be part of this Free Software community, they would
send an email to the project’s mailing list with new ideas. The answer
and reviews to this would be grateful and full of appreciation of the
work done by the contributor. When a Pull Request in this community is
reviewed, and the reviewers underline how grateful they are for the
effort of opening it in the first place. Imagine a Free Software
community that is thankful for their contributors, developers and of
course their maintainers. Such a community can easily become reality,
by simply saying "Thank you!"

The larger a Free Software project gets, the harder it is to
maintain. On “I Love Free Software Day” we acknowledge the work done
by each other for Free Software, even more we appreciate the work done
for Free Software. We reach out and say "Thank you!" to all the people
maintaining and contributing to Free Software, to all the people
advocating for Free Software: **Thank you ♥**

By doing so, we aim at creating a welcoming community in which not
only seasoned enthusiasts feel safe to participate, but also new
people are encouraged to join. A simple “Thank you” can motivate
others to keep contributing to Free Software!

With a simple "Thank you!" we appreciate the work and effort put into
Free Software by its contributors

Free Software is not just any type of software: it is a community, it
is software that empowers users and it is out there for all of us to
**use**, **understand**, **share**, and **improve**. Let us foster the
growth of a lovely and welcoming Free Software community, by
celebrating the software that empowers us! **♥**
