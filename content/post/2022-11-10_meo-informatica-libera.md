---
title: "Le opportunità dell'informatica libera" 
subtitle: "Un saggio del Prof. Angelo Raffaele Meo"
date: 2022-11-10T11:17:24+02:00
draft: false
---

Pubblichiamo a seguire un saggio di [Angelo Raffaele Meo](https://it.wikipedia.org/wiki/Angelo_Raffaele_Meo), professore emerito del Politecnico di Torino, il quale descrive, alla luce della sua lunga esperienza, le opportunità che vengono offerte dall'informatica libera, partendo dalla cultura hacker, ripercorrendo la nascita e la crescita del software libero, di un nuovo modello di business dell'industria del software. Free hardware e open education sono solo alcune delle parole chiave che vengono analizzate e che portano al sogno italiano di un'informatica veramente libera.

<img src="/img/angelo-raffaele-meo.jpg" alt="Foto Angelo Raffaele Meo" width="1000">
*Fonte: Wikipedia*

<!--more-->

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Indice**

- [La cultura hacker](#la-cultura-hacker)
- [La nascita del software libero](#la-nascita-del-software-libero)
- [Nasce l'industria del software libero](#nasce-lindustria-del-software-libero)
- [Internet e IETF](#internet-e-ietf)
- [La storia del software libero in Italia](#la-storia-del-software-libero-in-italia)
- [Free hardware](#free-hardware)
- [Open education](#open-education)
- [Conoscenza libera. Il brevetto.](#conoscenza-libera-il-brevetto)
- [Le economie dei dati di corpi e anime. Il trionfo dei GAFAM.](#le-economie-dei-dati-di-corpi-e-anime-il-trionfo-dei-gafam)
- [Il sogno italiano dell'Informatica libera.](#il-sogno-italiano-dell-informatica-libera)

<!-- markdown-toc end -->

La cultura hacker
=================

Il software libero affonda le sue radici nella gloriosa storia della
cultura "hacker".

Questa storia, che inizia negli anni cinquanta e si consolida negli
anni settanta del secolo scorso, ha il suo epicentro geografico negli
Stati Uniti e, in particolare, nelle due aree di Cambridge
(Massachussetts) e del Nord della California. Attorno a prestigiose
istituzioni universitarie come il M.I.T. (Massachussetts Institute of
Technology) e le università di Stanford e Berkeley si verificò una
felice e fortunata congiunzione di intelligenze e esperienze, un
fiorire di attività tecnologiche sul piano imprenditoriale e sociale
che accelerarono i tempi della rivoluzione informatica.

Mentre la rivoluzione della tecnologia informatica si diffondeva in
tutto il mondo, iniziava a manifestarsi anche una nuova rivoluzione
culturale con implicazioni importanti dal punto di vista sociale e
politico.

"Hack" è l'accetta che serve ad aprire la strada in una selva oscura;
"hacker" è lo studioso informatico che si apre la strada nella selva
oscura di un nuovo prodotto software per comprendere come è fatto. Lo
hacker ha il dovere assoluto di non danneggiare il prodotto che sta
studiando. (Non confondiamo quindi lo hacker con il "cracker" o il
generatore di virus, come spesso succede a qualche studioso
superficiale). L'obiettivo dello hacker è comprendere i principi di
scienza dell'informazione su cui è fondato un prodotto nuovo, al fine
di diffondere e rendere universale la conoscenza scientifica.

Il linguaggio degli hacker è simpatico e suggestivo. "Hack, hack" vuol
dire "ciao"; "I am hacking" significa "io sto studiando". La desinenza
"-p" di un noto linguaggio di programmazione indica una variabile
booleana che può assumere soltanto i due valori "vero" oppure "falso";
comunque l'algebra di Boole usata nel linguaggio ordinario deve essere
rispettata rigorosamente, per cui la domanda del cameriere "caffè-p OR
tè-p" è sbagliata perché ha sempre la risposta "vero".

Alcune unità lessicali o proposizioni standard hanno valore
ideologico.  Ad esempio, "suit" è uno scomodo abito da lavoro
caratterizzato da uno "strangling device" che riduce il flusso del
sangue al cervello (la cravatta) e, per estensione, "suit" è anche
l'individuo che porta lo scomodo abito di lavoro comprensivo di
cravatta, tipicamente il burocratico capoufficio. "Situation normal,
all fucked-up" ossia "tutto in ordine; tutto va del ..." (non si
possono scrivere parole volgari in un articolo per l'Accademia delle
Scienze) riassume una feroce critica del capitalismo.

La nascita del software libero
==============================

Il 1983 può essere considerato come l'anno del battesimo del software
libero, il figlio più importante del movimento degli "hacker". Padrino
di quel battesimo fu Richard Matthew Stallman, un ricercatore del
leggendario Laboratorio di Intelligenza Artificiale di M.I.T. In
quell'anno Richard Stallman svolgeva le funzioni di sistemista del
calcolatore del laboratorio e Xerox forniva i servizi di una grossa
stampante (9700 "Dover") a tutti i gruppi di ricerca del laboratorio.
Stallman prese consapevolezza dei disservizi di quella stampante; ad
esempio spesso si manifestavano congestioni nelle code di stampa per
cui egli pensò che quei problemi avrebbero potuto essere risolti con
piccole modifiche del codice della stampante. Pertanto Stallman chiese
alla Xerox il codice sorgente per integrarlo con alcune nuove linee di
codice, ma l'azienda rifiutò in forza di una legge del 1976 chiamata
"Copyright Act", che era stata rafforzata nel 1980, con specifico
riferimento al software, dal "US Software Copyright Act".

Quel rifiuto indusse Stallman a comprendere che la posizione dei
venditori di software proprietario di vietare ogni modifica di un
prodotto era contrario agli interessi dei cittadini, alla
collaborazione e all'apprendimento. Pertanto egli propose il concetto
di software libero come manifestazione della conoscenza libera,
patrimonio collettivo dell'umanità.

Nel settembre di quello stesso 1983 Stallman annunciò l'avvio di un
progetto di ricerca finalizzato allo sviluppo di un sistema operativo
libero, concorrente con il noto sistema operativo proprietario "Unix"
della A.T.&T. Il nuovo sistema operativo fu chiamato GNU sulla base
della seguente definizione ricorsiva (in linea con una tradizione
"hacker"): "GNU is Not Unix". Ossia: "GNU non è lo Unix della A.T.&T.,
ma è compatibile con questo".

Poco dopo Stallman fondò una organizzazione "no profit" chiamata "Free
Software Foundation" con l'obiettivo di dare una infrastruttura legale
al movimento del software libero. Nell'ambito di questa organizzazione
Stallman pose le basi teoriche per il perseguimento degli obiettivi
del movimento.

Al centro dell'ideologia di Stallman stanno quattro libertà
fondamentali:

1.  la libertà di eseguire il programma in qualunque contesto, per
    qualsiasi scopo;
2.  la libertà di studiare come funziona il programma e di modificarlo
    in modo da adattarlo alle proprie esigenze;
3.  la libertà di ridistribuire copie del programma al fine di aiutare
    altri programmatori;
4.  la libertà di migliorare il programma e di distribuirne
    pubblicamente i miglioramenti, in modo che tutti ne traggano
    beneficio.

Ovviamente, l'esercizio della seconda e della quarta proprietà impone
che il codice sorgente di qualunque programma sia sempre disponibile.

Pertanto un programma di software libero può essere venduto e/o
sviluppato su commessa a pagamento. Stallman sintetizza questo
concetto con la seguente affermazione: «Free as in "free speech"
(fondamento della costituzione americana) , not as in "free beer"»,
ossia «free come in "parola libera ", non come in "birra gratis"».

Poco dopo l'avvio del progetto GNU, Stallman lascia il M.I.T. con il
quale comunque continuerà la collaborazione. Prosegue il suo intenso
lavoro tecnico-scientifico sul nuovo sistema operativo e su altri
progetti, ma inizia come attività prevalente il suo apostolato del
software libero che lo vede ancora oggi protagonista assoluto a
livello mondiale.

Le sue conferenze sono tuttora molto interessanti, anche perché
provocatorie e spesso divertenti, come quando si presenta al pubblico
con una lunga tonaca nera e la superficie attiva di un hard disk della
prima generazione sul capo a mo' di aureola, per rappresentare se
stesso come San I-GNU-zio.

Inoltre Stallman lavora, dal duplice punto di vista giuridico e
tecnologico, su una bozza di licenza che chiama "copyleft", come
"diritto di copia di sinistra", in contrapposizione con il noto
"copyright" o "diritto di copia di destra". Da quella proposta
deriveranno, nell'arco di trenta anni, molte nuove proposte di
"copyleft" su cui non è opportuno soffermarsi per l'enorme complessità
ideologica e giuridica della questione. .Nel momento in cui scrivo
questo articolo -- maggio 2021 -- la sua figura leggendaria sta
attraversando un momento terribilmente negativo. Infatti, alcuni
colleghi importanti hanno chiesto la sua espulsione dalla Free
Software Foundation essendo state diffuse antiche notizie di molestie
sessuali da lui esercitate nei confronti di collaboratrici. Inoltre,
gli si rimprovera il fatto di aver difeso il grande miliardario
statunitense Jeffrey Epstein morto suicida in carcere dove era stato
rinchiuso con l'accusa di gravi reati di pedofilia.

Comunque, i meriti scientifici di Richard Stallman non possono essere
messi in discussione.

Nasce l'industria del software libero
=====================================

Richard Stallman e i suoi collaboratori realizzano componenti
importanti per GNU, ma non completano mai il nucleo del nuovo sistema
operativo.  Pare che il responsabile principale del ritardo sia
proprio Richard perché molto spesso s'innamora di un'idea nuova e
abbandona il lavoro già sviluppato per buttarsi, anima e corpo, nella
nuova variante del progetto.

Fortunatamente, nel 1991, uno studente 21-enne dell'università di
Helsinky, Linus Torvalds, sviluppa il nucleo di un nuovo sistema
operativo concorrente con UNIX e porta a compimento il sogno di
Stallman. Infatti, mentre la primissima licenza del nuovo sistema
operativo ha le caratteristiche del copyright, la versione 0.12 del
febbraio 1992 viene pubblicata con la GNU General Public License.

In onore del suo giovanissimo padre, il nuovo sistema operativo viene
chiamato Linux. Linux diventerà il simbolo del nuovo comparto
industriale del software libero.

Come Stallman ha sempre affermato si può fare business con il software
libero, vendendo il lavoro per lo sviluppo su commessa di uno
specifico prodotto, oppure sviluppando personalizzazioni in funzione
di determinate applicazioni, oppure ancora vendendo servizi di
assistenza tecnica o formazione. Alcune famiglie di prodotti sono note
quasi come le loro famiglie concorrenti di prodotti proprietari. Si
citano, ad esempio, BSD (Berkeley Software Distribution), variante
come Linux del sistema operativo Unix, nata nell'università di
Berkeley, in California; GCC (GNU C Compiler, nella prima versione,
poi trasformato in GNU Compiler Collection), traduttore
multipiattaforma dal linguoggio sorgente al linguaggio macchina;
MySQL. un gestore di basi di dati relazionali disponibile sia con
licenza GNU, sia con licenza proprietaria della multinazionale Oracle.

Sono disponibili in rete alcune piattaforme ed alcuni siti
specializzati nell'offerta di strumenti per lo sviluppo di software
libero. Il più antico e più noto è chiamato SourceForge che annovera
attualmente quattro milioni di programmatori su mezzo milione di
progetti diversi.  Gli utilizzatori di questi moduli software sono più
di 60 milioni; ogni giorno si registrano oltre 3,5 milioni di
download.

Forse come conseguenza della scelta di privilegiare i progetti
statunitensi, SourceForge è stata scavalcata da altre piattaforme,
come GitHub che annovera oltre 30 milioni di programmatori e 100
milioni di progetti diversi. Sulla base di questi dati si può
affermare che la dimensione globale del software libero è superiore a
quella del software proprietario, anche se il valore di mercato del
software proprietario rimane imbattuto.

Internet e IETF
===============

Poco prima dell'avvento del software libero, il governo degli USA,
probabilmente in risposta al successo sovietico rappresentato dallo
Sputnik, deliberò di istituire ARPA o "Advanced Research Progect
Agency" che si diede come primo obiettivo la realizzazione di una
grande rete mondiale di comunicazione. I militari sognavano una grande
rete di controllo per le forze di terra, di mare e dell'aria, in
contrapposizione con la comunità scientifica e accademica che invece
sognava una rete intergalattica della scienza. Prevalsero gli
accademici per merito prevalente del presidente di ARPA James Killian.

Nacque così ARPANET che generò Internet.

L'intera comunità scientifica mondiale fu coinvolta nel progetto sotto
la guida di una specifica struttura di coordinamento chiamata IETF
("Internet Engineering Task Force "). I principi adottati da IETF
appaiono oggi meravigliosamente suggestivi.

In primo luogo, per partecipare all'attività di IETF non è necessario
far parte di una struttura accademica, ma si può collaborare a titolo
personale.

In secondo luogo, i principi scientifici adottati, le soluzioni
innovative proposte e i programmi sviluppati devono essere condivisi.

Recitava l'IETF: "All we need is rough consensus and running code",
ossia "tutto ciò di cui abbiamo bisogno è un accordo di massima sugli
obiettivi perseguiti e programmi su calcolatore che funzionino
bene". E ancora "Fly before buy", ossia "vola con la fantasia prima di
buttarti su una soluzione già nota". Infine, a proposito dei documenti
da presentare a IETF: "Scrivili nel gabinetto, ma scrivili precisi e
chiari".

La grande rivoluzione di Internet , una delle più importanti nella
storia della scienza, è frutto dei principi del software libero. Non è
vero quel dogma secondo il quale il progresso è frutto prevalente
della competizione nel mercato; la collaborazione è molto più
efficiente.

La storia del software libero in Italia
=======================================

Nell'anno 2003 il ministro Lucio Stanca del secondo governo Berlusconi
costituì una commissione col compito di indagare sulle opportunità per
la pubblica amministrazione centrale e periferica rappresentate
dall'avvento del software libero. Il ministro mi affidò il compito di
presiedere quella commissione, probabilmente come premio per il libro
che avevo scritto con Mariella Berra sul tema "Informatica solidale",
libro che simpaticamente il ministro chiamava "Libretto rosso di Meo",
visto come seguito del più noto "Libretto rosso di Mao". Proprio la
questione della sicurezza fu al centro di una riflessione favorevole
al software libero e contraria al software proprietario.  Infatti
pochi giorni prima del dibattito era stato segnalato che il codice di
un noto prodotto proprietario conteneva una backdoor (o "porta di
servizio") attraverso le quali l'autore del prodotto poteva
segretamente accedere all'informazione relativa alla pubblica
amministrazione. Per evitare questo tipo di pericolo la commissione
richiese che l'offerta di qualunque prodotto proprietario contenesse
anche il codice sorgente del prodotto stesso in modo tale da
consentire il controllo dell'assenza di backdoor.

La proposta centrale che la commissione suggerì al ministro riguardava
la valutazione comparativa fra software libero e software proprietario
dal duplice punto di vista tecnico ed economico. Inoltre la
commissione propose che un prodotto finalizzato ad uno specifico
ambito applicativo di una pubblica amministrazione fosse pagato una
volta sola e quindi potesse essere ridistribuito, in tutto o in parte,
ad altre amministrazioni interessate. Infine si suggerì che gli
standard dei documenti e dei dati relativi alle interazioni fra le
pubbliche amministrazioni centrali e periferiche fossero
aperti. Questi oggetti, la documentazione relativa e soprattutto i
codici sorgente dei programmi liberi si propose dovessero costituire
un nuovo grande archivio liberamente consultabile dalle pubbliche
amministrazioni.

Dal lavoro della commissione derivarono gli articoli 68 e 69 della
legge 82/05 nota come CAD o "codice dell'amministrazione digitale",
che si riportano qui sinteticamente:

----------------------

**Articolo 68**

*Analisi comparativa delle soluzioni*

1.  Le pubbliche amministrazioni: acquisiscono, secondo le procedure
    previste dall'ordinamento, programmi informatici a seguito di una
    valutazione comparativa di tipo tecnico ed economico tra le seguenti
    soluzioni disponibili sul mercato:

	a) sviluppo di programmi informatici per conto e a spese
    dell'amministrazione ...

    b) riuso di programmi informatici sviluppati per conto e a spese
    della medesima o di altre amministrazioni;

    c) acquisizione di programmi informatici di tipo proprietario
    mediante ricorso a licenza d'uso;

    d) acquisizione di programmi informatici a codice sorgente
    aperto;

    e) acquisizione mediante combinazione delle modalità di cui alle
    lettere da a) a d)

1.  Le pubbliche amministrazioni, nella predisposizione o
    nell'acquisizione dei programmi informatici, adottano soluzioni
    informatiche che assicurino l'interoperabilità e la cooperazione
    cooperativa secondo quanto previsto dal decreto legislativo ..., e
    che consentano la rappresentazione dei dati e documenti in più
    formati, di cui almeno uno di tipo aperto, salvo che ricorrono
    peculiari ed eccezionali esigenze.

----------------------

**Art. 69**

*Riuso dei programmi informatici*

1. Le pubbliche amministrazioni che siano titolari di programmi
   applicativi realizzati su specifiche indicazioni del committente
   pubblico hanno obbligo di darli in formato sorgente, completi della
   documentazione disponibile, in uso gratuito ad altre pubbliche
   amministrazione che li richiedano e che intendano adattarli alle
   proprie esigenze, salvo motivate ragioni.

2. Al fine di favorire il riuso di programmi informatici di proprietà
   delle pubbliche amministrazioni ... i programmi appositamente
   sviluppati per conto e a spesa dell'amministrazione siano
   facilmente portabili su altre piattaforme.

----------------------

I testi degli art. 68 e 69 erano sostanzialmente allineati con le
conclusioni della commissione nominata dal ministro Stanca, con
un'unica eccezione: la commissione all'unanimità aveva richiesto che
soltanto standard aperti potessero essere accettati per
l'interoperabilità delle pubbliche amministrazioni centrali e
periferiche mentre invece l'articolo 69 parlava di *"... più formati
di cui almeno uno aperto ..." I* rapporti comunitari poco tempo dopo
(per opera di Nellie Kroes) ci diedero ragione all'unanimità: *"La
commissione Europea non può affidarsi ad un solo fornitore e non deve
accettare standard chiusi ... È una questione di democrazia".*

La legge 82/05 non produsse i risultati sperati. Ad esempio, nel 2006 il
sistema economico italiano registrò importazioni di prodotti software
proprietari per un valore dell'ordine di un miliardo di dollari ed
esportazioni di un valore trascurabile.

Per questa ragione il ministro Nicolais costituì una seconda commissione
con il compito di identificare strumenti e procedure di carattere
pratico per promuovere lo sviluppo del software libero. A me fu affidato
l'incarico di presiedere anche quella seconda commissione.

Gli obiettivi della commissione furono definiti dalla senatrice Magnolfi
nei termini seguenti:

1.  Sostenere la diffusione del software libero all'interno delle
    pubbliche amministrazioni centrali e periferiche;
2.  Elaborare linee guida normative, supporti tecnici, gruppi di
    eccellenza;
3.  Tutelare i responsabili dei sistemi informativi che scegliessero
    software libero;
4.  Potenziare le community di software libero;
5.  Creare sinergie con i ministri dell'industria e della scuola;
6.  Aggiornare eventualmente la legge 82/05, ossia il codice
    dell'amministrazione digitale.

La commissione produsse un rapporto di 300 pagine, ma a questo punto
scattò la maledizione di Bill Tutankhamon secondo la quale qualunque
uomo pubblico si occupi favorevolmente di software libero è destinato
a una immediata scomparsa dalla scena politica. Il governo cadde e il
ministro Nicolais fu sostituito dal ministro Renato Brunetta che
dichiarò che l'argomento non era di alcun interesse per lui, si
rifiutò di riceverci e buttò il nostro rapporto nel cestino.

Il codice dell'amministrazione digitale fu ritoccato varie volte
nell'arco della storia. La leggina più favorevole al software libero,
opera di un paio peones che avevano approfittato della sonnolenza
prodotta dall'elevata temperatura nell'aula, fu la legge 134/2012 che
recitava:

"L'acquisto di software in licenza proprietaria sarà possibile soltanto
quando la valutazione comparativa abbia dimostrato l'impossibilità di
accedere a soluzioni in software libero o già sviluppate dalla pubblica
amministrazione ad un prezzo inferiore".

Corse subito ai ripari il presidente del Consiglio Mario Monti, che si
era già scontrato con Microsoft in ambito comunitario e forse era
consapevole della maledizione di Bill Tutankhamon. Infatti la legge
2021/2012 proposta dalla Presidenza del Consiglio, ad una prima
lettura potrebbe apparire come una soluzione intermedia fra gli
articoli 68 e 69 del codice dell'amministrazione digitale e le
posizioni di Microsoft.

Ad esempio, una specifica importante recitava: "Livello di utilizzo di
formati dati e di interfacce di tipo aperto nonché di standard in
grado di garantire l'interoperabilità e la cooperazione
applicativa". Ora, per colpa della semantica talvolta ambigua della
congiunzione "nonché", alcuni ritengono che sia legittimo uno standard
non libero che garantisca l'interoperabilità e la cooperazione
applicativa.

Comunque, a meno di variazioni quasi irrilevanti, gli articoli 68 e 69
del codice dell'amministrazione digitale che abbiamo sopra riportato
sono tutt'ora validi ed è indiscutibile l'obbligo della relazione
comparativa che è ignorato dalla grande maggioranza delle delibere della
pubblica amministrazione del nostro paese relative all'acquisizione del
software proprietario.

I problemi del bilancio di molte pubbliche amministrazioni, il codice
dell'amministrazione digitale e alcune iniziative pubbliche e private
hanno indotto una decina di importanti comuni e alcune regioni a emanare
norme o promuovere provvedimenti a favore del software libero.

Certamente alla cultura dominante nel nostro Paese possiamo attribuire
qualche colpa per questo declino. Qui paghiamo la fortuna di essere
figli dell'impero romano, di essere stati la patria del diritto, di
possedere un patrimonio artistico e letterario come pochissimi altri
Paesi. Quindi la cultura dominante è umanistica ed ha influenzato anche
la cultura economica che non si è fatta sedurre dall'informatica.
Riporto le opinioni di alcuni personaggi importanti che hanno influito
sulle scelte del nostro Paese. Sono tutti ministri o politici di primo
piano, con l'eccezione di due autorevoli economisti e di un noto
filosofo e scrittore, il più feroce oppositore dell'informatica, che ho
chiamato GURU n° 8.

**GURU n° 1** (nel 1995): "Non c'è futuro per INTERNET"

**GURU n° 2**: "La tecnologia è una commodity che si può comperare al
supermercato e non ha senso spendere denaro per produrla"

**GURU n° 3**: "Il treno delle tecnologie dell'informazione per il nostro
Paese è irrimediabilmente perduto. Non dobbiamo spenderci neppure una
lira"

**GURU n° 4**: "La produzione di personal computer è divenuta la cenerentola
del settore dell'informatica. Occorre uscirne subito e chiudere lo
stabilimento di Scarmagno."

**GURU n° 5 e quasi tutti i GURU**: "E' urgente la privatizzazione della
Telecom".

**GURU n° 6**: "Nella scuola non più insegnamento della tecnologia, ma uso
della tecnologia per l'insegnamento."

**GURU n° 7**: "La fuga dei nostri giovani all'estero dimostra che
l'università italiana ha fallito. Continuare a finanziare un fallimento
è una follia."

**GURU n° 8**: "La scienza non divora. Sbrana. L'informatica puzza. Di
cadavere."

Certamente quelle opinioni hanno influito su molte scelte di politica
industriale che si sono rivelate negative dal punto di vista dello
sviluppo dell'informatica nel nostro Paese. Comunque, a mio giudizio, la
ragione più importante del nostro declino sta nella natura intrinseca
dell'industria delle anime (come il software) che è molto diversa
dall'industria dei corpi (come lo hardware).

 La prima importante differenza è rappresentata dai costi di
progettazione e sviluppo rispetto alla dimensione del prodotto. E' ben
noto che il prodotto industriale classico, ossia un corpo, è
caratterizzato da economie di scala rispetto alla dimensione del
prodotto. Un aeroplano da 500 passeggeri costa meno di due aeroplani da
250 passeggeri ciascuno; una petroliera da 100.000 tonnellate costa meno
di due petroliere da 50.000. La stessa economia di scala si manifesta
sulle dimensioni degli apparati produttivi: una fabbrica che produca un
milione di autovetture all'anno costa meno di due fabbriche da mezzo
milione di autovetture ciascuna.

Viceversa, il costo di produzione di un programma da 10.000 istruzioni è
più del doppio del costo di un programma da 5.000 istruzioni. Infatti,
al crescere delle dimensioni di un programma cresce il numero dei
sottoprogrammi da collegare, cresce clamorosamente il numero delle
interconnessioni di questi moduli, cresce il numero delle cose di cui i
programmatori debbono tener conto a mente, cresce il caos nella loro
testa e ancor più nel gruppo di progettisti che sta sviluppando il
prodotto.

Probabilmente il costo dello sviluppo di un programma cresce con il
quadrato delle sue dimensioni, per cui il prodotto da diecimila
istruzioni costa quattro volte il prodotto da cinquemila. E certamente
il numero dei bachi cresce ancora più rapidamente, forse con il cubo
delle dimensioni, per la difficoltà crescente di collegare
correttamente le unità elementari del programma.

Il prezzo sul mercato di un corpo, ossia di un'unità di prodotto di
tipo tradizionale, come un televisore o un'automobile, in virtù delle
economie di scala dei processi produttivi e distributivi diminuisce al
crescere del numero di oggetti venduti. In altri termini i settori
industriali classici sono caratterizzati da un un'economia di scala
anche rispetto alle dimensioni del mercato. Tuttavia, questa economia
è limitata da uno zoccolo duro, costituito dal costo delle materie
prime e dell'energia impiegate nella generazione di un'unità di
prodotto.

Nell'industria delle anime, come ad esempio nell'industria del
software, lo zoccolo duro non esiste perché il floppy disk o il cd rom
che ospitano il programma, o la carta di un giornale nel caso
dell'industria editoriale, o l'energia necessaria per irradiare un
programma televisivo, hanno un valore intrinseco molto piccolo. Di
conseguenza, il valore sul mercato di un'anima ossia, tipicamente, di
un prodotto software, di una trasmissione televisiva o di un giornale
è una funzione rapidamente decrescente del numero di copie vendute (o
di telespettatori). Se lo sviluppo di un prodotto software è costato
un milione di euro, il prezzo dell'unità di prodotto è pari almeno a
un milione di euro se vendo una copia sola, ma scende a mille euro se
riesco a venderne mille copie.

L'associazione perversa della diseconomia del costo della
progettazione e dello sviluppo sulla scala della dimensione del
prodotto e dell'accentuata economia di scala rispetto alla dimensione
del mercato produce poi la peculiarità più importante del mercato dei
prodotti dell'informazione. Per raddoppiare un programma che abbia già
raggiunto un certo successo sul mercato, si deve investire 4 volte di
più di quanto si era investito nella prima versione, ma per continuare
a venderlo allo stesso prezzo si deve poter contare su un mercato 4
volte più grande.

Questo fatto ha molte conseguenze. Ad esempio nell'arco di circa
quindici anni Bill Gates è diventato l'uomo più ricco della Terra,
mentre migliaia di software house in tutto il mondo, e in particolare
nel nostro Paese, chiudevano i battenti, oppure rinunciavano a operare
nel settore della produzione del software per ricreare comparti di
nicchia nell'area dei servizi di installazione o personalizzazione dei
prodotti altrui. Gli Stati Uniti hanno portato a casa migliaia di
miliardi di dollari con la vendita di prodotti software caratterizzati
da un tasso di valore aggiunto pari al 100%, mentre Paesi come il
nostro, che pur rivendicano una presenza nel novero dei Paesi più
industrializzati, non riescono ad esportare praticamente nulla in
questo comparto. E non parliamo dei Paesi poveri.

Free hardware
=============

Nell'anno 2005 un piccolissimo gruppo di ricercatori ed insegnanti di
Ivrea decise di sviluppare un micro calcolatore di basso costo
orientato prevalentemente alle esigenze didattiche. S'incontravano
spesso in un bar della loro città, chiamato "Arduino, re d'Italia", in
ricordo del sovrano che nel 1001 si era proclamato re d'Italia, anche
se le dimensioni del suo regno erano molto piccole. Per ricordare il
luogo dei loro incontri e il sovrano di mille anni prima decisero di
chiamare "Arduino" la scheda elettronica che avevano sviluppato.

Poiché quella scheda era stata pensata prevalentemente per
applicazioni didattiche, i suoi progettisti decisero di descriverla
pubblicamente nei minimi dettagli, dai componenti elementari
all'architettura delle loro interconnessioni e agli strumenti per la
programmazione e la liberalizzazione delle conoscenze. Nacque così, in
Italia, il "free hardware" o "hardware libero".

Da quella scheda altre ne seguirono, caratterizzate da funzionalità e
costi diversi. Olivetti e Telecom Italia fondarono un istituto
chiamato "Interaction Design Institute" con obiettivi di progettazione
e formazione.

La piattaforma Arduino trovò applicazione non soltanto nella scuola ma
anche nell'industria, soprattutto nell'area dei sistemi di controllo.
Così, oltre alle schede di elaborazione e controllo sono nate schede
per l'attuazione di funzionalità di sensori e attuatori.

Anche il software di base per la programmazione delle varie
piattaforme Arduino è assolutamente libero e gratuito. Così è nato un
nuovo comparto industriale, caratterizzato da centinaia di progetti
diversi e milioni di pezzi venduti ogni anno.

Open education
==============

Al concetto e alla logica del software libero è strettamente collegata
la cosiddetta "Open Education" che potremmo tradurre in "Scuola Aperta".

Secondo alcuni studiosi la storia della scuola aperta può essere fatta
risalire al Medio Evo, quando alcuni monasteri avviarono iniziative per
insegnare gratuitamente a leggere ai cittadini più poveri. Di importanza
fondamentale fu poi ovviamente l'invenzione della stampa.

La Open Education moderna nasce poco più di venti anni or sono, con
l'esplosione dell'importanza dell'informatica e di Internet in
particolare. Probabilmente la prima iniziativa forte nasce nel 2001
per opera del M.I.T. L'obiettivo di questa iniziativa, chiamata "MIT
OpenCourseWare (MIT OCW)", è rendere disponibile in rete,
gratuitamente, tutto il materiale didattico dei corsi sia di livello
"undergraduate" sia "graduate". La licenza adottata, proveniente dal
mondo del software libero, è "Creative Commons
Attribution-NonCommercial-ShareAlike licence".

In sintesi, il nome dell'autore del materiale utilizzato deve essere
sempre ricordato; è vietata la vendita del prodotto; è lecito produrre
nuovi oggetti derivanti dal materiale acquisito in Rete, ed è vietato
l'uso commerciale del nuovo prodotto.

M.I.T. ha reso disponibili migliaia di corsi. Di molti è disponibile il
filmato delle lezioni che può essere visualizzato in tempo reale oppure
scaricato dal sito per una visualizzazione successiva.

L'esempio di M.I.T. è stato adottato da molte altre università o
strutture accademiche nel mondo, per cui si pensa che siano milioni gli
uomini che hanno costruito parte delle loro conoscenze sull'Open
Education.

Il materiale distribuito -- libri di testo, documenti, lezioni,
conferenze, MOOC (Massive Open Online Courseware), strumenti didattici
vari -- costituisce le cosiddette OER (Open Education Resources) secondo
la seguente definizione proposta da UNESCO:

"Le OER sono materiale per l'apprendimento, l'insegnamento e la
ricerca, disponibili su qualunque supporto, digitale o meno, che
risieda nel pubblico dominio oppure sia stato rilasciato sotto una
licenza aperta che permetta l'accesso e l'uso gratuito, nonché
l'adattamento e la ridistribuzione da parte di altri soggetti con
nessuna o limitate restrizioni".

Anche in Italia sono attive alcune iniziative finalizzate alla
produzione di OER.

Ricordo, ad esempio, il progetto RAISAT Nettuno, divenuto nel 2014
Uninettuno University TIV, e i tre progetti attuali RAI Cultura, RAI
Scuola e RAI Play.

Ricordo anche l'associazione "Open Education Italia" che annovera un
centinaio di partecipanti; il volume "Open Education: OER, MOOC e
pratiche didattiche aperte verso l'inclusione digitale educativa" di
Fabio Nascimbeni; il "Manuale dell'e-Learning" di Matteo Uggeri.

Nel nostro piccolo, noi del cosiddetto "Gruppo Didattico", operante
presso il Politecnico di Torino, abbiamo sviluppato come supporto al
nostro portale FARE per la didattica a distanza (fare.polito.it), un
archivio di circa 2000 OER orientate prevalentemente all'insegnamento
dei fondamenti concettuali dell'informatica nelle scuole primarie e
secondarie. Mi scuso per questa operazione di marketing giustificata
dal fatto che tutto il nostro materiale è assolutamente libero e
gratuito, senza alcuna restrizione.

Conoscenza libera. Il brevetto.
===============================

Un giorno Papa Francesco ha enunciato la seguente equazione
matematica: "Capitalismo = demonio". Non essendo un economista non
sono in grado di discutere questa equazione, ma alcuni avvenimenti e
alcuni dogmi del pensiero economico dominante mi inducono a pensare
che il demonio esista e che il suo spirito entri nella testa degli
uomini anche più intelligenti.

Ad esempio, com'è possibile che nelle bozza del PNRR (il Piano
Nazionale di Ripresa e Resilienza, molto discusso in questo periodo)
qualche illustre personaggio abbia enunciato uno dei dogmi del
capitalismo e abbia scritto: "*A farci uscire dalla crisi saranno
concorrenza e mercato, contribuendo anche a una maggiore giustizia
sociale?*".

Io penso che la concorrenza produca inutili duplicazioni di lavoro. Ad
esempio penso, ma non posso dimostrare, che se i progettisti dei
vaccini anti-Covid avessero lavorato tutti insieme, collegandosi in
remoto, avrebbero impiegato meno tempo e avrebbero prodotto un vaccino
più sicuro.

Come secondo esempio dell'evidenza della presenza del demonio, mi
domando come sia possibile che Angela Merkel, figura leggendaria per
intelligenza ed onestà, a proposito della proposta Diden di abolizione
dei brevetti sui vaccini abbia affermato: "*La protezione della
proprietà intellettuale è una fonte di innovazione e deve rimanere
tale anche in futuro*".

In termini generali, a mio giudizio, basta l'esistenza dell'Istituto
del brevetto per dimostrare l'esistenza del demonio.

La conoscenza scientifica e tecnologica cresce con legge esponenziale
e ha raggiunto dimensioni che è molto difficile anche solo
immaginare. Per questo gli uffici brevetti, al fine di operare
correttamente, dovrebbero assumere milioni di operatori. Non potendolo
fare approvano qualunque proposta, per cui i brevetti attualmente
vigenti sono molti milioni.

Per convincere il mio lettore, lo invito a collegarsi ad un motore di
ricerca e a digitare due parole chiave: la parola "patent" e il nome
di qualunque cosa. Ad esempio, una ricerca divertente e significativa
si ottiene scrivendo "Patent, toilet" che produce molte centinaia di
risultati. La maggior parte delle soluzioni proposte è banale come il
gabinetto attaccato al portapacchi dell'automobile o come il vaso
nascosto sotto uno dei sedili dell'auto. A mio giudizio la soluzione
scientificamente e tecnologicamente più interessante è rappresentata
da un dispositivo studiato per evitare che i maschietti facciano la
pipì fuori dal perimetro del vaso. L'unità centrale di elaborazione
del sistema, sulla base dei dati provenienti da un insieme
bidimensionale di sensori, calcola l'epicentro del getto e produce un
segnale di allarme quando questo epicentro si avvicina pericolosamente
al perimetro del vaso.

Poiché negli Stati Uniti, che sono il paese più importante dal punto di
vista economico, una causa brevettuale costa un milione di dollari al
giorno, l'istituto del brevetto giova soltanto agli operatori più ricchi
e potenti. Qui appare chiara l'esistenza del demonio.

Le economie dei dati di corpi e anime. Il trionfo dei GAFAM.
============================================================

Un paio di mesi or sono mi sono collegato a un motore di ricerca per
conoscere l'ora di un incontro internazionale di calcio che
coinvolgeva la Juventus. Quel giorno non avevo ancora studiato a fondo
il così detto "capitalismo della sorveglianza", com'è stato chiamato
dalla nota studiosa Shoshana Zubof, e ho molto apprezzato il fatto che
un servizio importante mi era stato regalato.

Comunque, il gestore di quel motore di ricerca potrebbe nel suo mega
archivio avere associato al mio nome, oltre a molte altre
informazioni, anche il dato "bianconero". Allo stesso modo potrebbe
aver associato il dato "granata" al nome di Marco Mezzalama,
vicepresidente dell'Accademia delle Scienze e aver distribuito quelle
etichette ed altre ancora come "giallorosso", "nerazzurro", eccetera,
a molti altri cittadini. Poi forse oggi il motore di ricerca ha
venduto, ad una grande azienda che produce magliette sportive,
l'informazione relativa alla distribuzione sul territorio nazionale
dei tifosi bianconeri, granata, nerazzurri, eccetera.

Se il noto progetto chiamato "superlega", promosso da alcune società
calcistiche come la Juventus, avesse avuto successo, io mi sarei
vergognato di essere bianconero e quindi non sarei contento che
quell'informazione fosse stata distribuita in Rete.

Giustamente il GDPR si è sempre schierato contro i pericoli
rappresentati dalle "back door" di cui si parla da più di venti anni,
ossia le "porte di servizio nascoste" che consentono al gestore di un
programma applicativo di catturare informazioni personali degli
utilizzatori di quel programma.

Un esempio significativo e molto importante per le implicazioni sul
mercato del software, ossia delle anime, ma anche dei corpi, è
rappresentato dalla nota sentenza chiamata "Schrems II" della Carta di
Giustizia Europea.

Come è noto, il ricercatore Edward Snowden aveva rivelato che Facebook
e altri operatori sulla Rete partecipavano al programma "PRISM"
attivato dal governo degli Stati Uniti per la sorveglianza di
massa. Sulla base di quelle informazioni, nell'anno 2013 Maximilian
Schrems, un attivista austriaco, presentò una denuncia al garante
della protezione dei dati personali irlandesi lamentando che i suoi
dati personali, come quelli di altri cittadini dell'Unione Europea,
fossero stati trasmessi agli Stati Uniti d'America sulla base
dell'accordo chiamato "Safe Harber".

Si aprì una prima fase di dibattito che si chiuse nel 2016 con la
firma di un accordo chiamato "Privacy shield" che consentiva alle
autorità statunitensi l'uso di dati personali provenienti dall'Unione
Europea a condizioni relativamente severe.

Schrems non si rassegnò e continuò la sua battaglia legale sino alla
vittoria completa ottenuta nel luglio 2020, quando la Corte di
Giustizia Europea produsse la nota "sentenza "Schrems II" che
dichiarava "non valido" l'accordo Privacy Shield. In teoria questa
sentenza dovrebbe rappresentare un eccezionale punto di forza del
software libero in quanto l'adozione di software proprietario che
preveda il trattamento di dati personali dovrebbe essere
vietata. Comunque, per il momento, questa sentenza è generalmente
ignorata.

L'inosservanza della legge ha permesso l'esplosione del mercato
bianconero dei dati. Molte applicazioni dell'intelligenza artificiale
sono efficaci soltanto se costruite su grossi volumi di dati. Inoltre
l'industria dei dati è caratterizzata da economie di scala ancora più
importanti di quelle dell'industria del software. Infatti,
l'industriale che produce magliette sportive non si accontenta di
conoscere il numero di bianconeri che abitano a Torino, ma ha bisogno
di conoscere la distribuzione sull'intero territorio nazionale dei
bianconeri, dei granata, dei nerazzurri, e così via, compresi i tifosi
del Benevento. Se in un certo momento il possessore della banca dati
sa tutto sulla distribuzione dei tifosi, tutti i produttori di
magliette si rivolgeranno a lui e non resterà spazio per nessun altro
gestore di dati.

Le clamorose economie di scala che caratterizzano l'industria
informatica e in particolare l'industria dei dati spiegano il valore
economico di cinque imprese che in questo momento dominano i loro
mercati. Sono Google, Amazon, Facebook, Apple e Microsoft, indicate
nel loro insieme con la parola GAFAM.

Google ha una capitalizzazione superiore a 1000 miliardi di dollari,
12 volte più grande della capitalizzazione di General Motors, la più
importante industria automobilistica americana, che produce milioni di
autovetture all'anno.

Amazon ha una capitalizzazione ancora superiore, dell'ordine di 1500
miliardi di dollari.

La capitalizzazione di Facebook, che annovera una media di 2,8
miliardi di utenti mensili, supera 750 miliardi di dollari.

La capitalizzazione di Apple supera 2000 miliardi di dollari ed è
quindi dell'ordine di grandezza del prodotto interno lordo del nostro
Paese, ossia della somma dei valori dei prodotti e dei servizi buttati
in campo in un anno. Secondo alcuni economisti la capitalizzazione di
Apple potrebbe superare 3000 miliardi di dollari nell'arco di un paio
di anni.

Infine, anche la capitalizzazione di Microsoft, che secondo alcuni
studiosi sembra attraversare un momento di crisi, è superiore a 2000
miliardi di dollari.

L'esistenza del demonio è dimostrata dai dati economici dei GAFAM, che
nessun economista può spiegare in modo convincente.

Tutti i GAFAM fatturano centinaia di migliaia di dollari al minuto.

E' molto preoccupante il fatto che al dominio economico dei GAFAM si
associa anche un altrettanto potente dominio culturale.

Ha scritto giustamente, a mio giudizio, il sociologo Mauro Fioroni:
"il dominio economico dei GAFAM è capace di rendere invisibile o
annientare la diversità culturale ed epistemica. Il loro dominio si
basa su un ecosistema di dispositivi, applicazioni e media che da un
lato favoriscono l'immaginazione di comunità raccolte attorno ad
un'esperienza del mondo dettata dagli algoritmi, dall'altro
conquistano ogni spazio, dalla vita privata degli individui, valore
aggiunto alla ricchezza delle multinazionali della rete".

Il sogno italiano dell'Informatica libera.
==========================================

Nella seconda metà degli anni '70, in una serie di studi approfonditi
sulla realtà mondiale, personaggi molto noti della cultura, della
politica e dell'industria di quegli anni intravidero nell'avvento
delle tecnologie dell'informazione un'opportunità di progresso per i
paesi in via di sviluppo. Ricordiamo, ad esempio, il rapporto Brandt,
titolato "Nord-Sud: un programma per la sopravvivenza", promosso da
McNamara, allora presidente della Banca Mondiale; il memoriale
"Mitsubishi", frutto di un lavoro congiunto di alcune decine di
studiosi occidentali e giapponesi; due rapporti al Club di Parigi e al
Club di Roma di Peccei; il famoso volume "La sfida mondiale" di Jean
Jacques Servan-Schreiber.

Quei rapporti furono tutti caratterizzati da un grande ottimismo,
ispirato dalla constatazione che le tecnologie dell'informazione hanno
un contenuto intrinseco di materie prime ed energia praticamente
trascurabile. Essendo il contenuto di quelle tecnologie puramente
intellettuale ed essendo l'intelligenza umana distribuita nella stessa
misura su tutti i popoli della terra (come osservava Cartesio), le
stesse opportunità di sviluppo tecnologico ed economico avrebbero
dovuto aprirsi al paese ricco e a quello povero.

Già nei primi anni del nuovo millennio, a quaranta anni di distanza
dal momento in cui uomini animati da acuta intelligenza e ideali
forti, come Brandt, Mc Namara, Shiller, Mitsubishi, Schreiber, Peccei,
avevano sognato un futuro migliore, basato sulle nuove tecnologie e
sull'industria dell'informazione e costruito su una stretta
collaborazione internazionale, i rapporti OCSE non soltanto rilevavano
che il divario tecnologico, industriale ed economico fra Paesi ricchi
e Paesi poveri non era diminuito, e anzi era cresciuto, ma anche
osservavano che era molto cresciuta la differenza del reddito dei
cittadini ricchi e di quello dei cittadini poveri che vivono in tutti
i paesi, compresi i paesi ricchi. Le stesse tecnologie
dell'informazione si erano diffuse prevalentemente nei paesi del Nord.

Ora il demonio si sta adoperando per aggiungere una "z" alla sigla
GAFAM, che pertanto diventerà GAFAMZ. Infatti, negli ultimi due anni è
esplosa l'importanza economica di Zoom, l'azienda americana che offre
al mercato una delle più importanti piattaforme per le videoconferenze
e le videolezioni. In virtù di 300 milioni di utenti al giorno e di un
fatturato dell'ordine di 700 milioni di dollari al trimestre, il
valore in borsa di Zoom ha raggiunto 150 miliardi di dollari, pari
alla somma dei valori delle sette più importanti aziende aeronautiche
del mondo.

Parallelamente è esploso il terribile fenomeno chiamato "zoombombing".
Un malintenzionato, oppure, più spesso, un gruppo di malintenzionati,
si infiltra in una videoconferenza o videolezione al fine di
arricchirla di insulti ignobili, di immagini o filmati pornografici,
di messaggi politici impresentabili. Negli ultimi mesi sono stati
oggetto di zoombombing, secondo i quotidiani, una conferenza
femminista, una manifestazione in ricordo dell'Olocausto, un dibattito
sul ruolo della Cina nella società moderna. Lo zoombombing ha colpito
anche moltissime lezioni scolastiche e sono stati introdotti filmati o
immagini pedopornografiche durante lezioni per bambini della scuola
primaria.  Inoltre, secondo New York Times, Zoom contiene gli
strumenti per l'acquisizione e il trasferimento ad altri soggetti di
dati personali degli utenti.

In considerazione dei problemi rappresentati dallo zoombombing e dai
pericoli della violazione dei dati personali, il Senato degli USA, i
governi di Australia, Germania, India, Taiwan, il noto Federal Bureau of
Imvestigation e molte imprese internazionali vietano ai loro cittadini o
dipendenti l'uso di Zoom. Al contrario, Zoom condivide con Google il
primato del numero di scuole italiane loro clienti.

Sfortunatamente, il declino economico dei Paesi poveri riguarda anche il
nostro Paese.

Il nostro prodotto interno lordo è dell'ordine di 1700 miliardi di
dollari, nettamente inferiore a quello di quasi tutti i Paesi europei;
ad esempio, il prodotto interno lordo della Germania è esattamente il
doppio del nostro.

Il prodotto interno lordo pro capite appare drammaticamente basso,
poiché è dell'ordine di 25.000 dollari mentre quello dell'Irlanda è pari
a 60.000 dollari, più del doppio del nostro.

Il rapporto del debito pubblico rispetto al prodotto interno lordo, che
nell'anno 2000 era pari a 1,2 ha raggiunto oggi quota 1,55, pari ad
oltre 1,5 volte la media europea. Si pensa che il costo dei
provvedimenti anti-Covid porterà ad un ulteriore drammatico incremento
di quel rapporto.

Abbiamo visto che l'Italia occupa una delle ultime posizioni nella
classifica europea dei livelli di digitalizzazione; questo dato dimostra
che l'incremento pericoloso del nostro debito pubblico non è dovuto a
investimenti in ricerca.

Non sono un economista; inoltre sono affetto da un basso valore del
"felicity growth factor", come lo chiamo io, ossia sono un pessimista. I
dati che leggo in rete relativi all'economia del nostro Paese mi
inducono a pensare che il nostro sia un "Paese in via di sottosviluppo".

Oggi l'informatica libera rappresenta l'unico strumento disponibile per
il progresso tecnologico ed economico dei Paesi poveri ed anche di un
Paese come il nostro. Infatti, l'adozione di un prodotto proprietario
implica una spesa onerosa, generalmente verso un Paese straniero, con
grave danno per la bilancia commerciale, senza alcun beneficio dal punto
di vista scientifico-tecnico, essendo sufficiente imparare da un manuale
l'uso dello strumento.

Invece, una spesa per un prodotto del software libero, generalmente
inferiore del corrispondente prodotto del software proprietario, può
essere interpretato come un investimento in ricerca e sviluppo. Infatti,
la spesa per acquisire quel prodotto dal mercato o per sviluppare un
prodotto nuovo di assegnate funzionalità è rappresentata quasi
totalmente dal costo del personale nazionale che deve studiare la logica
informatica di quel prodotto, il codice di un linguaggio avanzato di
programmazione, l'organizzazione del programma già disponibile o in fase
di nuovo sviluppo.

In Italia è attiva un'industria micro-elettronica non molto importante
a livello internazionale, ma di altissimo livello tecnologico. Nel
quadro del PNRR il ministro Colao ha proposto un importante
potenziamento della rete nazionale per la trasmissione dei
dati. Sinceramente, conoscendo i miei polli, temo che parti importanti
del PNRR siano affidate a multinazionali straniere, ma sogno che
quella sia una rete prevalentemente pubblica e che l'organizzazione
sia esclusivamente o quasi esclusivamente nazionale. Siamo in grado di
produrre i componenti micro-elettronici necessari, e conosciamo
perfettamente l'architettura di una rete per la trasmissione dei dati,
come dimostra la fantastica rete italiana a banda ultralarga, dedicata
all'istruzione, alla ricerca e alla cultura, chiamata GARR (Gruppo di
Armonizzazione Reti per la Ricerca). Inoltre sogno che i componenti
microelettronici prodotti dalla nostra industria siano "hardware
free". I clamorosi successi degli "Arduino" cinesi dimostrano che
quella scelta potrebbe essere una magnifica opportunità di business
anche per il nostro Paese.

Il PNRR potrebbe rappresentare un'importante opportunità per una
conversione almeno parziale all'informatica libera.

Le difficoltà per l'attuazione di questa piccola rivoluzione
tecnologica e industriale non sono di natura economica ma
culturale. Infatti la cultura dominante nella classe dirigente del
nostro Paese, indipendentemente dalla collocazione politica a sinistra
oppure a destra, è figlia delle opinioni degli otto guru che ho sopra
riportato.  Le proposte dei militanti della parrocchia del software
libero sono sistematicamente ignorate.

Ad esempio, nel mese di giugno dell'anno scorso, avevo inviato un
messaggio alla ministra Azzolina in cui mi permettevo di rimproverarla
perché sul sito del ministero si raccomandavano i prodotti dei soliti
noti per l'attuazione della didattica a distanza e questo era, a mio
giudizio, una grave violazione della legge. Quella mia lettera fu
presentata e discussa in termini positivi dalla senatrice Maria Laura
Mantovani in una seduta del Senato. Speriamo che la maledizione di
Bill Tutankhamon non colpisca la senatrice Mantovani in occasione
della prossima elezione. Comunque, a quella lettera non fu data alcuna
risposta e le raccomandazioni sul sito del ministero non furono
rimosse.

Nel gennaio scorso, essendosi aperto il dibattito sul PNRR che era
allora chiamato "Recovery Plan", nel timore che quel progetto
diventasse un'opportunità di business per i soliti noti, per noi
onerosa, scrissi come responsabile della parrocchia del software
libero al Presidente Conte e ai suoi ministri competenti una lettera
importante che fu sottoscritta da un centinaio di ricercatori e da una
decina di associazioni. La più importante delle richieste contenute in
quella lettera riguardava il rispetto delle norme di legge come il
codice dell'amministrazione digitale o come la disciplina nazionale e
comunitaria per la protezione dei dati personali.

A quella lettera rispose soltanto la ministra Paola Pisano, che
ricordò l'attività da lei svolta nell'area del software libero,
manifestò il suo accordo con la nostra affermazione secondo la quale
l'adozione di software libero è un obbligo di legge per la pubblica
amministrazione e in particolare per la scuola, e dichiarò la sua
disponibilità a collaborare per l'attuazione della rete nazionale
della teledidattica basata esclusivamente sul software libero.

Molto significativa mi pare la seguente affermazione della ministra:
"*Il next generation EU rappresenta un'importante opportunità per
sostenere il nostro Paese rilanciando l'economia e stimolando gli
investimenti privati. Il dipartimento metterà a disposizione tutte le
risorse necessarie perché questo avvenga nel pieno rispetto di privacy
e trasparenza, favorendo l'adozione di soluzioni "open-source" e
riducendo il "vendor-lock"*".

A questo punto, come era ovvio, scattò la maledizione di Bill
Tutankhamon. Il governo Conte fu sostituito dal governo Draghi e la
ministra Pisano non fu chiamata a far parte del nuovo governo
nonostante i molti importanti contributi da lei portati, come, ad
esempio, la creazione del grande archivio "Designers Italia" del
software già disponibile alla pubblica amministrazione italiana.

Ovviamente inviammo al presidente Draghi e ai nuovi ministri
competenti una lettera in cui si ribadivano i concetti discussi nella
lettera inviata pochi mesi prima al governo Conte. Altrettanto
ovviamente, nessuna risposta alla nostra lettera è mai arrivata e la
logica delle nostre proposte è assolutamente assente nella bozza del
PNRR predisposta dal nuovo governo.

Una decisione significativa dell'impostazione data al PNRR dal nuovo
governo è la composizione del "comitato consultivo per la transizione
amministrativa per la pubblica amministrazione" nominato dal ministro
per la pubblica amministrazione Renato Brunetta. Quel comitato è
composto da 20 direttori o importanti esponenti delle principali
pubbliche amministrazioni centrali e locali del nostro paese e da un
unico tecnico che non è un esponente del software libero, come noi
avremmo desiderato, ma il direttore della divisione "Pubblica
Amministrazione" di Microsoft Italia.

Una conseguenza della assenza degli esponenti del governo nazionale
dall'analisi delle opportunità rappresentate dall'Informatica libera è
costituita dal fatto che la maggioranza delle scuole e delle strutture
scientifiche italiane adotta illegalmente il software di Google o di
Zoom per attuare video lezioni o video conferenze remote. In varie
occasioni ho tentato di convincere i responsabili di quelle scelte di
passare alle nostre soluzioni, ma senza successo.

Quasi sicuramente questo mio articolo sarà letto soltanto dai
confratelli della parrocchia del software libero. A loro rivolgo
quindi questa mia conclusione.

Stiamo vivendo una drammatica involuzione della cultura e
dell'industria mondiale, con implicazioni negative per i paesi e i
cittadini più poveri, ed anche, in una misura significativa, per il
nostro Paese.  Anche il grande economista Paul Rober, premio Nobel,
che per anni era stato considerato "la rockstar dell'industria
tecnologica", secondo la definizione di Wall Street, ha compreso la
realtà e si è convertito. Ha ferocemente criticato se stesso e i
colleghi economisti per aver fornito una copertura intellettuale a
questa drammatica involuzione. Ha osservato che i suoi colleghi
economisti pensano: "E' il momento. Non si può far nulla. E' il
mercato", ma ha affermato con forza: "E' sbagliato".

Un altro grande dell'economia, Moshe Vardi, ha scritto a sua volta: "Le
tecnologie ci stanno guidando verso il futuro. Ma chi c'è al volante?".

La risposta mi pare ovvia: "Al volante c'è il governo nei suoi vari
livelli, dalla Presidenza del Consiglio ai sindaci dei piccoli
Comuni."

Noi della parrocchia del software libero, nell'interesse dei nostri
figli e nipoti, abbiamo il dovere di spiegare agli uomini del governo,
e, ad esempio, a quanti stanno lavorando sul PNRR, che la rivoluzione
dell'informatica libera può aiutare a superare questa drammatica
involuzione.

Come ha scritto Maria Laura Mantovani nella conclusione del suo
discorso al Senato: "*Siamo chiamati alla transizione digitale da
percorrere in tempi ridotti; facciamolo decisamente puntando sulle
nostre risorse.  Sovranità digitale significa mettere a sistema i
nostri cervelli, le nostre infrastrutture e i nostri dati per
rilanciare l'economia del paese*".

