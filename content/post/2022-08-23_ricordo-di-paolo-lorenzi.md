---
title: "Un pensiero a Paolo Lorenzi" 
subtitle: "Il ricordo dell'amico Piergiorgio Cemin"
date: 2022-08-23T12:33:03+02:00
draft: false
---

<img src="/img/paolo-lorenzi.jpg" alt="Paolo Lorenzi" width="400">

Il 26 giugno scorso ci ha lasciato, dopo una malattia incurabile durata circa un anno, Paolo Lorenzi: ispettore scolastico per il settore matematico scientifico e tecnologico dal 2000 al 2013, è stato l’artefice e il padre, insieme a Paolo Zilotti, allora collaboratore della Sovrintendenza per i sistemi informatici delle scuole, della coraggiosa scelta di installare software libero su tutte le postazioni informatiche utilizzate per la didattica nella scuola italiana. 

<!--more-->

Nel pensiero di Paolo Lorenzi la motivazione principale per questa scelta non era tanto il risparmio, quanto piuttosto la maggiore opportunità data dal software libero di poter vedere il codice dei programmi e quindi di poterli studiare ed eventualmente modificare a vantaggio di tutta la comunità.

*"A spingerci verso il software libero non ci sono ragioni economiche legate ai costi delle licenze proprietarie. L’unica molla che ci ha spinto al cambiamento è stato un approccio per così dire filosofico che seguiamo nei processi di istruzione. Crediamo che le tecnologie abbiano un ruolo fondamentale nella costruzione dei saperi e poter contare su tecnologie non proprietarie consente di allargare le possibilità di crescita".*

Questo diceva Paolo Lorenzi nel settembre del 2005 in una intervista a Punto Informatico, nel motivare la  scelta, che lui ha promosso con tutte le sue forze, del software libero nella scuola italiana dell’Alto Adige. 

La caratteristica fondamentale scelta da Paolo nel progettare questo passaggio al software libero è stata quella di coinvolgere direttamente un gruppo di insegnanti, già attivi nella gestione delle aule di informatica, che sono stati preparati con un corso di formazione di sei mesi in cui essi hanno imparato a gestire le aule di informatica con il nuovo sistema e  a dare supporto agli insegnanti nell’utilizzo delle tecnologie informatiche: non insegnanti coinvolti a tempo pieno solo nell’attività informatica, ma che sono rimasti parzialmente in attività anche come insegnanti specifici delle loro materie. L’obiettivo era, quindi, quello di avere degli insegnanti capaci di supportare i colleghi nell’azione didattica con le nuove tecnologie, per renderli nel tempo autonomi nell’utilizzo delle stesse.

Paolo ha sempre fondato la sua azione prima di insegnante, poi di ispettore, nella convinzione che l’insegnamento della matematica, delle scienze e della tecnologia, dovesse sempre passare attraverso  la  sperimentazione e lo stimolare la curiosità. A un amico, lontano da interessi scientifici e tecnologici per formazione ricevuta, che gli chiedeva, circa un mese prima della morte, cosa trovasse di così interessante nella matematica rispose: 

*"La matematica insegna grandi cose, quanto mai utili alla vita. La pazienza e la costanza, ad esempio, di fronte ad un problema: occorre provare e riprovare, verificare una intuizione e non scoraggiarsi se non porta da nessuna parte; sospendere allora la ricerca senza indispettirsi ma anche senza rinunciare per tornare più tardi all’attacco tentando altre vie; e questo più volte, ammettendo le proprie sconfitte ma continuando a credere nel potere della ragione ragionante … fino a che, col sospirato successo, si sbocca su di un panorama che ti colma di stupore e ti regala una meritata soddisfazione".* 

Su questo pensiero è nato l’altro grande progetto di cui Paolo è stato ideatore e fondatore: la Bottega del Matematico. Nata nel 2003, il progetto si poneva l’obiettivo di mettere a confronto le eccellenze scolastiche della provincia più interessate all’ambito scientifico con dei docenti universitari in una esperienza che somigliasse alle botteghe artigiane del 1500. La casa “Haus Noldin” a Salorno si è rivelata il posto adatto per ospitare questa esperienza, nella quale gli studenti, sotto lo stimolo e la guida di docenti universitari provenienti dalle Università di Trento e Milano, cercavano insieme di risolvere dei problemi molto spesso non affrontati nel percorso didattico scolastico, assaporando il metodo della ricerca in modo simile a quello che avveniva nelle antiche botteghe artigiane. E questo avveniva in quattro giorni di vita in comune, in cui esperienza manuale nella manipolazione di oggetti, uso e ricerca con le nuove tecnologie, portavano gli studenti, attraverso continuo confronto e lavoro di gruppo, a costruire concetti e nuove conoscenze. 

Tralascio la mia profonda amicizia con Paolo. Tutti coloro che come me sono stati contagiati dal suo entusiasmo per un diverso approccio alla conoscenza scientifica ne sentono la mancanza.

Ciao Paolo e grazie di tutto.

*Piergiorgio Cemin*
