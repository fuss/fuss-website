---
title: "Smart working con Software Libero" 
subtitle: "FUSS usato anche in amministrazione"
date: 2020-04-17T20:14:07+02:00
draft: false
---

![smart-working](/img/smart-working-1.jpg)
*Nadia Fasciano, vicedirettrice del III Circolo didattico delle scuole dell'infanzia a Bolzano.* 

Lavorare da casa in *smart working*, denominato nel nostro ordinamento "lavoro agile", prevede che i dipendenti possano utilizzare i propri dispositivi tecnologici. Il prerequisito è che l'amministrazione deve essere in grado di fornire ai dipendenti strumenti che funzionino con qualsiasi sistema operativo. E' il caso di applicativi web per leggere la posta elettronica, per la produttività d'ufficio, per la fatturazione elettronica, per le videoconferenze, tanto per citarne alcuni. Quanto più l'amministrazione ha investito nello sviluppo e adozione di strumenti web accessibili da internet, tanto più agevole è il lavoro della/o *smart worker*. Esiste pur sempre la possibilità di collegarsi attraverso un canale sicuro (VPN) alla propria postazione di lavoro in ufficio al fine di poter accedere anche a quegli applicativi che non sono fruibili wia web da casa. Tale possiblità per questioni di licenze e banda è concessa soltanto a pochi dipendenti ed in linea di principio dovrebbe essere ridotta quanto più possibile.

<!--more-->

Lo smart working per essere tale deve permettere di usare gli stessi strumenti presenti nella postazione del proprio ufficio e ciò è tanto più facile quanto più sostenibile ed aperta è la via scelta dalla PA nell'adozione del software. Ad esempio non sarebbe possibile per una questione di licenze e costi dotare ogni dipendende anche a casa della stessa suite d'ufficio proprietaria Office365 installata in ufficio. La versione on-line di Office, inoltre, non ha le stesse funzionalità di quella installata sulla macchina. In tal caso, fortunatamente corre in aiuto la suite d'ufficio libera [LibreOffice](https://it.libreoffice.org) disponibile per tutti principali sistemi operativi e scaricabile gratuitamente da ogni dipendente senza alcun aggravio di costo. Non costituisce tra l'altro una novità per i dipendenti della Provincia di Bolzano, in quanto LibreOffice è installato in tutti gli uffici. 

Con la necessità di lavorare da casa si è manifestata l'esigenza di riutilizzare anche computer usati (principalmente notebook) presenti negli uffici ma inutilizzati per l'impossibilità di installarvi l'ultima versione di Windows, molto esigente in termini di risorse. Per ovviare a tale problema noto col nome di *obsolescenza programmata* imposta dalle multinazionali del software, anche in questo caso il software libero corre in aiuto. In particolare il sistema operativo GNU/Linux, essendo in grado di sfruttare appieno le risorse hardware di un PC, consente di ridare vita ai PC usati. [GNU/Linux FUSS](https://fuss.bz.it), usato per la didattica nelle scuole in lingua italiana, è stato installato su svariati notebook permettendo al personale della Provincia di lavorare in sicurezza con il software libero ed alla Provincia stessa di fare economia circolare senza alcuna spesa accessoria.

Sul territorio provinciale sono molte le aziende che dispongono di computer non più utilizzati per i motivi descritti sopra. E' per questo che l'associazione [LUGBZ](https://lugbz.org) si è attivata con il progetto [FUSS-PC](https://www.lugbz.org/la-didattica-demergenza/) recuperando tali PC con l'aiuto dell'associazione [ADA](https://www.adanazionale.it/sedi/) per la logistica e la sanificazione dei PC e del [Gruppo Digital Sustainability Südtirol-Alto Adige](https://openbz.eu) e degli [Sportelli Open & Linux](https://fuss.bz.it/page/desk/) del territorio per l'installazione di GNU/Linux FUSS.

