---
title: "Südtirols Netzwerk für Nachhaltigkeit" 
subtitle: "FUSS-Projekt wird Partner des Netzwerks"
date: 2020-08-25T21:14:03+02:00
draft: false
---

![Südtirols Netzwerk für Nachhaltigkeit](/img/snn.png)

Das **FUSS-Projekt** wird als [**Partner in das Südtirols Netzwerk für Nachhaltigkeit aufgenommen**](https://www.future.bz.it/netzwerkpartner/alle-partner/fuss-digital-sustainability-at-school).
Im Jahr 2019 erhielt FUSS den [Preis "Nachhaltige öffentliche Verwaltung"](https://fuss.bz.it/post/2019-05-15_premio-pa-sostenibile/), der von ForumPA und ASviS organisiert wurde. Nun sind wir Mitglieder dieses wichtigen Netzwerks.

<!--more-->

[Südtirols Netzwerk für Nachhaltigkeit](https://future.bz.it/) ist eine freie, autonome, partei- und interessensübergreifende Plattform mit einem klaren, dreifachen Auftrag:

1. Die 17 Nachhaltigen Entwicklungsziele der Vereinten Nationen, auch SDGs genannt, in Südtirol bekannt zu machen und deren Zusammenspiel und Bedeutung – global und für Südtirol – aufzuzeigen.
2. Die Zivilbevölkerung, d.h. Vereine, Organisationen und Gruppen, rund um diese Ziele zu vernetzen, im Sinne einer konstruktiven und wertschätzenden Auseinandersetzung.
3. Die Netzwerkpartner*Innen sowie deren Eigeninitiativen und Veranstaltungen mit Bezug zu diesen Zielen auf dieser Webseite sichtbar zu machen.

Netzwerkpartner*Innen sind Südtiroler Organisationen, Vereine und Gruppen, die als gemeinsames Ziel die Umsetzung der SDGs (nachhaltige Entwicklungsziele der UN) in Südtirol haben. In den Bereichen Soziales, Umwelt und Wirtschaft arbeiten wir auf diese Weise verstärkt auf eine nachhaltige Entwicklung hin.


