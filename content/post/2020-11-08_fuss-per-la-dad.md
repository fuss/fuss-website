---
title: "A casa come a scuola" 
subtitle: "Un sistema completo per la didattica digitale integrata"
date: 2020-11-08T11:14:03+02:00
draft: true
---

![Foto al Cornaro di Jesolo](/img/istituto-cornaro-jesolo-2020.jpg)
**Luca Marcolongo** (responsabile informatico), **Raffaele Trotta** (vicepreside) ed **Emilio Martin** (docente)

E' partita la Didattica Digitale Integrata; sia le scuole che le famiglie si debbono dotare di tutti gli strumenti informatici necessari per riuscire rispettivamente a condurre e seguire la didattica da remoto. Per seguire le lezioni da casa serve almeno un PC con webcam, un notebook oppure un tablet. PC e notebook sono tra i più richiesti in quanto permetto


<!--more-->




