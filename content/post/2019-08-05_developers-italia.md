---
title: "Riuso del software nella PA"
subtitle: "FUSS è il primo progetto della Provincia Autonoma di Bolzano pubblicato su Developers Italia"
date: 2019-08-05T23:33:12+02:00
draft: false
---

Il progetto FUSS detiene diversi primati. È il più grosso progetto nazionale di sostenibilità digitale nelle scuole e compie 14 anni a settembre di quest'anno. Dal 2005 tutto il software che viene usato nelle nostre scuole è libero e riusabile a norma dell'[articolo 69](https://docs.italia.it/italia/piano-triennale-ict/codice-amministrazione-digitale-docs/it/v2017-12-13/_rst/capo6_art69.html) del Codice dell'amministrazione digitale. E` inoltre il **primo progetto della Provincia Autonoma di Bolzano** che è stato pubblicato sulla piattaforma **[Developers Italia](https://developers.italia.it/it/software/p_bz-fusslab-fuss)**, punto di riferimento per il software della Pubblica Amministrazione, nel rispetto delle **[Linee Guida su acquisizione e riuso di software per le pubbliche amministrazioni](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/index.html)**.

<!--more-->