---
title: "MERITO STANDARDIZZAZIONE DIGITALIZZAZIONE: dove va la scuola italiana" 
subtitle: "Pubblicati gli atti del convegno tenutosi a Palermo"
date: 2023-05-04T21:53:14+02:00
draft: false
---

<img src="/img/banner_convegno_2023-05-04.webp" alt="Banner convegno 4 maggio 2023 - Merito, standardizzazione , digitalizzazione: dove va la scuola italiana">

Giovedì **4 maggio 2023** si è tenuto a Palermo presso l'IISS "Damiani Almeyda - Crispi" il convegno ["**Merito, standardizzazione , digitalizzazione: dove va la scuola italiana**"](https://cobasscuolapalermo.com/merito-standardizzazione-digitalizzazione-dove-va-la-scuola-italiana/) organizzato dal **CESP - Centro Studi per la Scuola Pubblica**.

Al convegno ha preso parte anche il **Progetto FUSS** quale modello di digitalizzazione sostenibile in grado di creare valore pubblico.

Sono disponibili al seguente link gli [**atti del convegno**](https://cobasscuolapalermo.com/merito-standardizzazione-digitalizzazione-dove-va-la-scuola-italiana/): [https://cobasscuolapalermo.com/merito-standardizzazione-digitalizzazione-dove-va-la-scuola-italiana/](https://cobasscuolapalermo.com/merito-standardizzazione-digitalizzazione-dove-va-la-scuola-italiana/).

<!--more-->
