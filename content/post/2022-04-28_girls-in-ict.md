---
title: "28 aprile: giornata internazionale 'Girls in ICT'" 
subtitle: "Il Progetto FUSS propone un'iniziativa per sostenere la presenza delle ragazze nell'ambito scientifico-tecnologico"
date: 2022-04-28T10:12:10+02:00
draft: false
---

![Girls in ICT - Cover](/img/girls-in-ict_cover.jpg)

Il Progetto FUSS, da sempre sensibile alle tematiche dell'inclusione, della libera condivisione della conoscenza e dell'abbattimento del divario digitale ha pensato di dare il proprio contributo alla sensibilizzazione di docenti, studentesse e studenti per promuovere la giornata internazionale ["**Girls in ICT**"](https://www.itu.int/women-and-girls/girls-in-ict/) cogliendo l'invito rivolto alle organizzazioni partner dall'iniziativa [**Repubblica Digitale**](https://repubblicadigitale.innovazione.gov.it/it/) del [**Ministero per l'innovazione tecnologica e la transizione digitale**](https://innovazione.gov.it/notizie/articoli/girls-in-ict-una-raccolta-di-iniziative-per-l-edizione-2022/).

<!--more-->
Dal **28 aprile** e per tutta la settimana verrà modificato lo sfondo di tutti i 4.500 computer in uso presso le 74 scuole in lingua italiana della Provincia Autonoma di Bolzano. Lo [sfondo](https://fuss.bz.it/girls-in-ict), creato per il progetto FUSS da Stevan Strauss De Castro, è liberamente disponibile con licenza GPLv3 (GNU General Public License) e verrà caricato anche sui PC e notebook forniti dalle scuole in comodato agli studenti per la didattica digitale integrata nonché sulle centinaia di computer forniti alle famiglie dal progetto [SchoolSwap](https://schoolswap.bz.it).

![Girls in ICT - Background](/img/girls-in-ict_background.png)
*L'immagine realizzata per il Progetto FUSS da Stevan Strauss De Castro per la giornata internazionale "Girls in ICT" (Licenza: GPLv3)*

![Logo Repubblica Digitale](/img/endorsement/repubblica-digitale.png)
