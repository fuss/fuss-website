---
title: "Workshop: PUAVO and FUSS" 
subtitle: "A Finnish and a South Tyrolean Free Software project meet to share their Digital Sustainability experience with schools."
date: 2023-06-03T12:23:00+02:00
draft: false
---

Don't miss this workshop on **Tuesday June 13th, 2023 from 17:00 to 19:00** where two twin Free Software projects, [**Puavo**](https://puavo.org) from Finland and [**FUSS**](https://fuss.bz.it) from South Tyrol get together to share their long experience of **Digital Sustainability** within schools.

<!--more-->

The workshop will take place in the Meeting Room (ground floor) at the **Intendenza Scolastica Italiana** and at the **Free University of Bozen-Bolzano** (streaming) in Room E2.22.

It will be possible to follow the event remotely at the link [**https://fuss.bz.it/puavo-fuss-conference**](https://fuss.bz.it/puavo-fuss-conference).

A [**recording**](https://ibz-bbb2.comeinclasse.it/playback/presentation/2.3/b5214b8fc36b8ff5c0347c50208f9a5fff3c2d04-1686666468103) of the workshop is available.

[<img src="/img/puavo-fuss-flyer.png" alt="Workshop flyer" width="600">](/material/puavo-fuss-flyer.pdf)

