---
title: "Didattica al cubo" 
subtitle: "Corso introduttivo su Minetest a cura di Marco Amato"
date: 2024-03-21T17:16:12+02:00
draft: false
---

<img src="/img/minetest-logo.png" alt="Minetest Logo" width="200">

**Lunedì 8 aprile 2024** dalle ore **17:00** alle **19:00** si terrà
presso il **Liceo Scientifico "Toniolo"** ed in contemporanea in
**videoconferenza** il corso "**Didattica al cubo**" sulle vaste
potenzialità didattiche di
[**Minetest**](https://www.minetest.net/). Il relatore, **Marco Amato
(Zughy)** è contributore e membro della squadra principale del
progetto.

**Minetest** è un applicativo *sandbox* basato su
[*voxel*](https://en.wikipedia.org/wiki/Voxel), oggetti 3D, molti dei
quali semplici cubi, comunemente chiamati "nodi" all'interno di una
griglia regolare. Il concetto di "voxel" di Minetest è simile a quello
dei blocchi da costruzione (come i blocchi LEGO). Nel mondo
interattivo si possono usare i "blocchi" per costruire oggetti e
attrezzi di uso quotidiano.

<!--more-->

![Minetest info screen](/img/minetest-info-screen.png)
Schermata informativa di Minetest.

Si possono costruire case, fattorie, città, strumenti e molte altre
cose incredibilmente creative a partire dai "blocchi" materiali. Ad
esempio, il minerale metallico viene estratto, intagliato in lingotti,
utilizzato per creare picconi o strumenti ed oggetti che vengono poi
utilizzati per costruire edifici o qualsiasi altra struttura che si
possa immaginare.

Nella sua versione base Minetest non ha un obiettivo predefinito e
lascia dunque molto spazio alla fantasia del giocatore nel creare
quello che più desidera.

I notevoli gradi di libertà offerti da Minetest lo rendono
particolarmente adatto a diverse attività didattiche
interdisciplinari, richiamate da diversi siti specializzati quali:

- Minetest for Education (https://www.minetest.net/education/)
- MinetestEDU (https://wiki.minetest.net/MinetestEDU/)
- Minetest Education Edition: https://github.com/edu-minetest/minetest

Minetest viene utilizzato in ambienti educativi per insegnare materie
come la matematica, la programmazione e le scienze della terra.

Non aggiungiamo altro, sperando di avervi incuriositi ed invogliati a
partecipare al corso.

E` disponibile la [**registrazione**](https://video.fuss.bz.it/w/q7CjcFqWZVBDaxegeuux7a) del seminario sulla istanza [**PeerTube di FUSS**](https://video.fuss.bz.it/w/q7CjcFqWZVBDaxegeuux7a).

[<img src="/img/2024-04-08_Minetest_fronte.png" alt="Volantino corso Minetest" width="600" border="1">](/material/2024-04-08_Minetest.pdf)
