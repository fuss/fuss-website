---
title: "La sostenibilità digitale a scuola nel pieno rispetto del CAD e del GDPR." 
subtitle: "Incontro con le scuole del Lazio"
date: 2023-06-26T23:58:06+02:00
draft: false
---

<img src="/img/digitale-nachhaltigkeit-icon.svg" alt="Icon Digital
Sustainabiity - Digitale Nachhaltigkeit" width="200">

Il **26 giugno 2023** dalle **17:00** alle **19:00** si è tenuto un
incontro informativo **online** per i dirigenti ed i docenti di più di
70 scuole del Lazio sulla scia di diversi altri interventi formativi
tenutisi in altre regioni d'Italia.

Segue il sommario ed il programma dell'evento.

<!--more-->

SOMMARIO
--------

I relatori presenteranno un approccio alle tecnologie informatiche per
le scuole che faccia leva su di un modello di condivisione
realmente democratico basato sul software libero, in grado di
accelerare enormemente il processo di innovazione tecnologica e
garantire la sicurezza dei dati personali secondo il GDPR e nel pieno
rispetto del Codice dell'amministrazione digitale.

PROGRAMMA
---------

Partecipa all'incontro **Guido Scorza**, componente del **Collegio del
Garante per la Protezione dei Dati Personali**.

Ideato e curato da **Massimo Corinti**. Promosso dalla rete “**ARETE**".

---

* 17:00 - **Introduzione**, Massimo Corinti, Data Protection Officer

* 17:05 - **Che volto avrà la scuola del futuro?**, Stefano Borroni Barale, docente, CUB SUR - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Barale.pdf)

* 17:20 - **Il progetto FUSS e la creazione di valore pubblico**, Paolo Dongilli, Progetto FUSS, Provincia Autonoma di Bolzano - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Dongilli.pdf)

* 17:35 - Intervento di **Guido Scorza**, componente del **Collegio del Garante per la Protezione dei Dati Personali**

* 17:55 - **Aspetti legali da considerare quando la PA acquisisce software**, Marco Ciurcina, avvocato - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Ciurcina.pdf)

* 18:10 - **L’impegno di Zorin per una scuola libera**, Albano Battistella, Zorin - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Battistella.pdf)

* 18:20 - **Cosa significa per le aziende lavorare con il modello del software libero e perché è la soluzione ideale per le scuole**, Tre testimonianze di aziende operanti sul territorio nazionale:
  * Christian Surchi, Truelite
  * Marco Marinello, QNets
  * Giorgio Favaro, Continuity - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Favaro.pdf)

* 18:35 - **Sostegno sul territorio a chilometro 0**, Associazioni nel Lazio:
  * LinuxShell Italia, Anna Pizzicato e Marco Zampetti - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_LinuxShell.pdf)
  * Linux Club Italia
  * Roma2LUG, Samuele Tanzini - [[SLIDE]](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Roma2LUG.pdf)

* 18:45-19:00 - **Domande e discussione**

[<img src="/img/2023-06-26_Incontro-Scuole-Lazio_Programma.png" alt="Programma Sostenibilità Digitale nelle scuole del Lazio" border="1">](/material/incontro-lazio/2023-06-26_Incontro-Scuole-Lazio_Programma.pdf)

VIDEO 
-----

* [Registrazione BigBlueButton](https://bbb9.comeinclasse.it/playback/presentation/2.3/38335f21855ce588b61002f4552e37639ac65c75-1687789616696)

