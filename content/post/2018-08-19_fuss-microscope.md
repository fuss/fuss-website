---
title: "Pronto l'applicativo fuss-microscope"
subtitle: "Sviluppato dallo studente Marco Marinello per pilotare il microscopio Intel Play QX3"
date: 2018-08-19T08:30:23+02:00
draft: false
---

Nella nuova distribuzione FUSS 9 ora è anche disponibile l'applicativo [`fuss-microscope`](http://archive.fuss.bz.it/pool/main/f/fuss-microscope/) sviluppato dallo studente Marco Marinello del [Liceo scientifico delle scienze applicate "Rainerum"](http://www.rainerum.it/liceo-scientifico-delle-scienze-applicate).

<!--more-->

Con questo applicativo è possibile pilotare il microsocopio [Intel Play QX3](https://en.wikipedia.org/wiki/Intel_Play) in dotazione nei laboratori di diversi istituti della Provincia di Bolzano.

[![Intel Play QX3 Microscope](/img/intel-play-qx3-microscope.jpg)](https://en.wikipedia.org/wiki/Intel_Play)

Ringraziamo di cuore [Marco Marinello](http://www.provincia.bz.it/news/it/news.asp?news_action=4&news_article_id=543511) per il suo [costante impegno](http://www.provincia.bz.it/news/it/news.asp?news_action=4&news_article_id=543511) su numerosi fronti nell'ambito del progetto FUSS ed in generale a favore dello sviluppo sostenibile delle tecnologie informatiche nella scuola ed anche nell'amministrazione.


