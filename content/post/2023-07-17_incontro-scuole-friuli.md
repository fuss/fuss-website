---
title: "CAD e GDPR, casi di successo a km 0" 
subtitle: "Soluzioni reali a confronto"
date: 2023-06-26T23:58:06+02:00
draft: false
---

[<img src="/img/evento_privacy_20230717_border.png" alt="Programma Evento Privacy Scuole Friuli Venezia Giulia" width="400">](/material/incontro-friuli/evento_privacy_20230717_volantino.pdf)

Su richiesta dei **responsabili della protezione dei dati (DPO) in difficoltà** nello svolgere le obbligatorie valutazioni di impatto (DPIA) per i dirigenti scolastici che hanno scelto google e microsoft, nascono una serie di eventi ad hoc, in molte regioni, che consentono al personale preposto di conoscere gli aspetti legali, tecnici e le soluzioni in uso presso scuole e aziende su tutto il territorio italiano. 

Le **soluzioni open source** per la didattica digitale, cui le PA, scuole pubbliche comprese, devono dare la priorità a norma dell'**art. 68 del CAD**, opportunamente integrate, sono una valida alternativa alle soluzioni proprietarie delle Big Tech e **garantiscono il diritto alla riservatezza dei minori** in quanto applicano i principi di **privacy by design** e **by default**.

Il **17 luglio 2023** dalle **16:45** alle **19:30** si terrà un nuovo evento informativo **online** per i dirigenti ed i docenti del Friuli Venezia Giulia.

Seguono il [**programma dell'evento**](/material/incontro-friuli/evento_privacy_20230717_volantino.pdf), gli slide e la registrazione del seminario.

<!--more-->

- [**Programma**](/material/incontro-friuli/evento_privacy_20230717_volantino.pdf)
- **Slide**:
  - **Marco Ciurcina**, avvocato: *Aspetti legali da considerare quando la PA acquisisce software (incluse le problematiche legate a google, microsoft etc)*, [[SLIDE]](/material/incontro-friuli/2023-07-17_Friuli_Ciurcina.pdf)
  - **Giorgio Favaro**, Continuity: *Soluzioni REALI con software libero di livello enterprise integrato, supporto ed assistenza inclusi*, [[SLIDE]](/material/incontro-friuli/2023-07-17_Friuli_Comeinclasse.pdf)
  - **Paolo Dongilli**, Progetto FUSS: *Alunni e docenti al centro grazie ad una digitalizzazione democratica e sostenibile*, [[SLIDE]](/material/incontro-friuli/2023-07-17_Friuli_Dongilli.pdf)
  - **Albano Battistella**, Zorin: *Zorin OS: un sistema per i computer degli studenti e degli insegnati, dedicato alla scuola alla sostenibilità ed alle comunità*, [[SLIDE]](/material/incontro-friuli/2023-07-17_Friuli_Battistella.pdf)
- **Video**: [**Link alla registrazione del seminario**](https://bbb9.comeinclasse.it/playback/presentation/2.3/9afce0f5f6314f5e36ec15d11aec17b6774fb554-1689603093910)
