---
title: "Upgrade alla Scuola di Musica 'Vivaldi'" 
subtitle: "Estesa l’infrastruttura della scuola ed aggiornato il sistema su server e PC"
date: 2021-03-01T00:14:03+02:00
draft: false
---

**a cura del prof. Fabio Michelini**

![Foto di un'aula didattica della scuola](/img/vivaldi_2021.jpg)

Si può fare didattica a distanza con la musica? La [**Scuola di musica in lingua italiana "Antonio Vivaldi"**](https://scuole-musica.provincia.bz.it) ha risposto a questa domanda affidandosi al progetto FUSS. Nelle aule, accanto agli strumenti musicali, sono state installate e messe in rete le postazioni con a bordo il sistema operativo basato su [Debian 10 "buster"](https://www.debian.org/) e tutta la dotazione di software open-source necessaria a fare musica e a mettere in contatto i docenti con gli allievi.

<!--more-->

La prima categoria di software utilizzati è costituita dalle applicazioni web che permettono di fare didattica in videoconferenza, come [EduMeet](https://edumeet.org/), [Jitsi Meet](https://jitsi.org/) e [BigBlueButton](https://bigbluebutton.org/). Alcune di queste piattaforme sono ospitate da centri di ricerca e università e raccolte nel portale [iorestoacasa.work](https://iorestoacasa.work). Accanto ai software per la didattica a distanza (DaD), vi sono quelli [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) di supporto alla produzione e alla distribuzione di materiale multimediale, come ad esempio l'editor di partiture [MuseScore 3](https://musescore.org/it) che rappresenta un'alternativa di livello ai software proprietari Finale e Sibelius.

La rete FUSS, già in uso nella scuola da diversi anni, è stata aggiornata e ampliata nella sede centrale di piazza Parrocchia 19, ma si prevede di estenderla anche alle numerose sedi secondarie che coprono tutta la regione, raggiungendo  un'utenza complessiva di circa 90 docenti e 3600 allievi. L'esperienza con il il Nucleo FUSS della Provincia Autonoma di Bolzano è stata, per la competenza e per il sostegno avuto, assolutamente positiva.

La formazione necessaria ai docenti di musica per l'utilizzo delle postazioni è stata realizzata in parte in videoconferenza e le istruzioni per avviare la DaD sulle diverse piattaforme sono state propagate dagli stessi  docenti, spesso con spirito volontaristico e *peer to peer*. Pur affezionati ai software specialistici della musica, i docenti si sono dimostrati più che disponibili a cambiare nella direzione dell'open-source una volta riconsiderate le tematiche della privacy, della sicurezza e della libertà di utilizzo. Per assecondare questo interesse, il Nucleo FUSS organizzerà una formazione di primo livello che aiuterà il personale, oltre che a praticare la DaD con sicurezza, a muoversi in un mondo digitale sempre più complesso, tutelando la privacy degli allievi.
