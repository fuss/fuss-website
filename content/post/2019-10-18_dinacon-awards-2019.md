---
title: "DINAcon Awards 2019"
subtitle: "FUSS is one of the three finalists as best practice project"
date: 2019-10-18T11:56:12+02:00
draft: false
---

![DINAcon Awards 2019 - best practice nominations](/img/dinacon-awards-2019.png)

The **DINAcon Awards** recognise and honour the courage and innovative power of open projects by communities, companies, administrations, organisations and individuals every year. Candidates from all over Europe are eligible to participate and for the second year in a row, [**FUSS**](https://awards.dinacon.ch/en/awards/fuss-free-upgrade-for-a-digitally-sustainable-school/) was nominated as one of the best projects, namely [**one of the three most voted projects in the category best practice**](https://awards.dinacon.ch/en/shortlist-is-out/). The FUSS Team is proud of this new achievement. Keep up the good work!
<!--more-->
