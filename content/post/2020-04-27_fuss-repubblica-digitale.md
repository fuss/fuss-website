---
title: "Repubblica Digitale" 
subtitle: "Il Progetto FUSS, membro della Coalizione Nazionale, entra in Commissione Europea"
date: 2020-04-27T21:25:07+02:00
draft: false
---

![repubblica-digitale](/img/repubblica-digitale-eu.png)

[**Repubblica Digitale**](https://innovazione.gov.it/it/repubblica-digitale/) è l'iniziativa con la quale l'Italia aderisce al [**programma della Commissione UE per le competenze e le professioni digitali**](https://ec.europa.eu/digital-single-market/en/national-local-coalitions). 

[**Il progetto FUSS fa parte di questa importante coalizione italiana**](https://repubblicadigitale.innovazione.gov.it/iniziativa/fuss-free-upgrade-for-a-digitally-sustainable-school---sostenibilita-digitale-nella-scuola/). 15 anni fa, nelle scuole di lingua italiana parte lo sviluppo del sistema didattico FUSS ("Free Upgrade for a digitally Sustainable School", tradotto "Sistema libero per una scuola digitalmente sostenibile"). FUSS sviluppa per le scuole della nostra Provincia un sistema open source, che si evolve nel tempo con il contributo degli studenti e in base alle necessità di chi lo utilizza: insegnanti e studenti. FUSS era *ante litteram* in linea con i principi del **Manifesto di Repubblica Digitale**:

- Educazione al digitale
- Cittadinanza digitale
- Digitale etico, umano e non discriminatorio

<!--more-->

[Il progetto FUSS, facendo parte ora di Repubblica Digitale](https://repubblicadigitale.innovazione.gov.it/iniziativa/fuss-free-upgrade-for-a-digitally-sustainable-school---sostenibilita-digitale-nella-scuola/), porta un modello di sviluppo digitale sostenibile nell'ambito dell’iniziativa strategica nazionale promossa dal Dipartimento per la trasformazione digitale della Presidenza del Consiglio dei ministri.

Il progetto FUSS si colloca con altri 108 progetti nazionali che fanno parte della Coalizione Nazionale del programma europeo per le competenze digitali, lanciato dal Ministro per l’innovazione tecnologica e la digitalizzazione Paola Pisano, nella sfida per raggiungere l'inclusione e sviluppare competenze digitali.

Gli obiettivi della coalizione sono infatti quelli di colmare le diverse forme, sociali e culturali, di divario digitale tra la popolazione italiana, favorire l’inclusione digitale e promuovere lo sviluppo di nuove competenze professionali.

Oggi le attività vedono coinvolti ministeri, Regioni, Anci, Upi, Confindustria, associazioni, università, ricerca, imprese, Rai e sono 24 i Paesi che partecipano al programma.

"In Italia è necessario ridurre il divario digitale nella forza lavoro, spesso più ampio che nella maggior parte degli altri paesi europei, attraverso l’aumento di competenze” - aveva affermato il Ministro Pisano in un comunicato stampa di inizio aprile - "e risolvere gli ostacoli, che impediscono l'efficacia di qualsiasi programma di trasformazione digitale".
