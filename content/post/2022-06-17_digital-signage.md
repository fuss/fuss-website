---
title: "Schermi informativi scolastici con Software Libero" 
subtitle: "Debian, Xibo, NextCloud e LibreOffice le soluzioni usate per fornire un nuovo tassello tecnologico alle scuole in un'ottica di riuso"
date: 2022-06-17T12:04:12+02:00
draft: false
---

![Digital signage alla Scuola di Musica 'Vivaldi' di Bolzano](/img/digital-signage-vivaldi.jpg)
Digital signage alla Scuola di Musica "Vivaldi" di Bolzano.

Sempre più scuole vogliono dotarsi di sistemi di **digital signage** (cartellonistica digitale) per poter fornire a docenti, studenti e visitatori informazioni mirate nel corso della giornata.
Le informazioni visualizzate riguardano la didattica e l'amministrazione scolastica e compredono avvisi, incontri giornalieri, eventi periodici, progetti scolastici con l'aggiunta di altre tipologie di messaggi quali news, meteo oppure contenuti multimediali.

<!--more-->
Il sistema fornito dal Nucleo FUSS di Sostenibilità Digitale nella didattica è installato nell'infrastruttura dell'intendenza scolastica italiana. Basato sul software open source [**Xibo**](https://xibo.org.uk/), permette di configurare per ogni scuola che ne faccia richiesta uno o più *display* a seconda dei monitor che si desidera installare. La configurazione di ciò che dev'essere visualizzato viene fatta comodamente attraverso un semplice browser da remoto.  Ciascun display può visualizzare uno o più *layout* programmati con un palinsesto. Ogni layout ha a disposizione numerose tipologie di elementi quali testi, immagini, PDF, video, audio, HTML, intere pagine web, calendari, dataset, RSS-feed (news) e molto altro.
Per la predisposizione di calendari che permettono di gestire le risorse condivise (aule) della scuola, viene fornito un account sull'istanza NextCloud del progetto FUSS con la quale Xibo è in grado di interfacciarsi.

Le scuole, pertanto, debbono solo dotarsi di un monitor di dimensioni adeguate e di un mini PC connesso alla rete da montare dietro al monitor. Sul PC viene installato Debian "FUSS" unitamente al software open source "Xibo Player".

La **Scuola di Musica "Vivaldi"** è stata la prima a fare uso di Xibo e NextCloud con la necessità di rappresentare le informazioni di ben 43 calendari per le aule di musica.

A seguire abbiamo coinvolto uno studente dell'IISS "Galileo Galilei", **Manuel Chiocchetti**, che ha svolto il suo PCTO in questo ambito. Con Manuel abbiamo raccolto le esigenze di due istituti (**Istituto Comprensivo Bolzano VI** – Scuola secondaria di secondo grado “U. Foscolo” ed il **Liceo Classico “G. Carducci”**) intervistando i rispettivi referenti tecnici. Sono state individuate le componenti che necessitavano di uno o più calendari creandoli sulla piattaforma NextCloud. Per l’IC Bolzano 6 si sono resi necessari 5 calendari (4 progetti scolastici e avvisi), mentre per il Liceo “Carducci” 3: un calendario condiviso, uno per gli eventi in aula magna ed uno per le supplenze.

![Digital signage al Liceo classico 'Carducci' di Bolzano](/img/digital-signage-carducci.jpg)
Digital signage al Liceo classico "Carducci" di Bolzano.

Manuel ha progettato abilmente nuovi layout pronti all’uso per entrambi gli istituti connettendo i calendari NextCloud agli stessi, unitamente ad altre componenti dinamiche (p.es. ticker news locali e meteo). Per il Liceo “Carducci”, invece, sono stati riprodotti esattamente i due layout esistenti, uno presente all’ingresso della scuola e l’altro per l’aula docenti.

Da fine maggio fino a metà giugno due nuovi studenti hanno scelto di svolgere il tirocinio del 4° anno presso il Laboratorio del Nucleo di Sostenibilità Digitale: **Alberto Iovino** dell'**IISS "G. Galilei"** e **Massimiliano Mola** della **TFO "Max Valier"**.

![Alberto Iovino e Massimiliano Mola al FUSS Lab](/img/digital-signage-lab.jpg)
Alberto Iovino e Massimiliano Mola presso il Laboratorio di Sostenibilità Digitale in Intendenza scolastica italiana

Interessati alla tematica sia dal punto di vista dell'architettura hardware e software della soluzione che da quello più creativo di ideazione di un infoscreen, Alberto e Massimiliano si sono adoperati per contattare due scuole di Bolzano: la **Scuola secondaria di primo grado "E. Fermi"** e la **Mittelschule "A. Egger Lienz"**. Hanno raccolto con pazienza dai referenti delle scuole i requisiti necessari, implementando due nuovi layout. Nelle seguenti immagini possiamo vedere le loro creazioni:

![Layout digital signage ICBZ4](/img/digital-signage-icbz4.jpg)
Layout predisposto da Alberto per la Scuola secondaria di secondo grado "E. Fermi"

![Layout digital signage MS Egger Lienz](/img/digital-signage-egger-lienz.jpg)
Layout predisposto da Massimiliano per la Mittelschule "E. Lienz"

Con il tempo restante, Alberto ha deciso di preparare un nuovo layout per la **Scuola secondaria di primo grado "V. Alfieri"**, permettendo ai docenti di gestire la tabella delle supplenze mediante **LibreOffice online** e caricando la stessa in Xibo.

Massimiliano, invece, ha raccolto la necessità della scuola "Egger Lienz" di avere anche un PC con monitor per la gestione dell'infoscreen. In un'ottica di **economia circolare digitale** è stato riutilizzato hardware dismesso da altre scuole ma perfettamente funzionante e rispondente all'esigenza dell'istituto. Sul PC e' stato installato il sistema operativo **Debian GNU/Linux FUSS** in modalità "standalone" unitamente al software **Xibo Player** per visualizzare l'infoscreen; dotato infine di disco veloce SSD e chiavetta USB wifi per la connessione alla rete.

![PC per digital signage MS Egger Lienz](/img/digital-signage-egger-lienz-pc.jpg)
Il PC fornito alla Mittelschule "E. Lienz" dopo l'installazione e l'avvio di Xibo Player

Anche per questi due tirocini come per il precedente è stato possibile trasferire agli studenti durante il PCTO un bagaglio di conoscenze che confidiamo potrà tornare utile per progetti e lavori futuri da loro stessi condotti nel campo del digital signage con il Software Libero.
