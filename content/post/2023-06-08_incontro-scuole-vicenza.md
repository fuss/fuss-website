---
title: "PNRR e Scuola 5.0. Consigli per investimenti rispettosi di CAD e GDPR." 
subtitle: "Incontro con le scuole della Provincia di Vicenza"
date: 2023-06-08T21:53:14+02:00
draft: false
---

<img src="/img/digitale-nachhaltigkeit-icon.svg" alt="Icon Digital
Sustainabiity - Digitale Nachhaltigkeit" width="200">

L'**8 giugno 2023** si è tenuto un incontro informativo per i dirigenti
ed i docenti di circa 150 scuole della Provincia di Vicenza su gentile invito del Provveditorato di Vicenza.

Considerata la situzione decisamente critica nella quale le scuole del
territorio nazionale si trovano a causa delle soluzioni in cloud
adottate nel corso degli ultimi tre anni in quanto non a norma del
GDPR, urge la necessità di informare le scuole su quali siano le
alternative viabili in grado di tutelare efficacemente i dati di
alunni e docenti. Questo il sommario ed il programma dell'incontro
(corredato da slide e video) che si è tenuto di pomeriggio dalle 17:00 alle 19:00.

<!--more-->

SOMMARIO
--------

L’Italia è fanalino di coda per competenze digitali e l’informatica. I
fondi del PNRR che sono stati erogati, possono essere delle grandi
occasioni per far si che le scuole possano diventare un polo
d’eccellenza a riguardo, investendo su sistemi e software
Open-Source.Inoltre,gli ultimi attacchi informatici, pongono oggi la
necessità, di investimenti a tal riguardo.

Perché sostenibilità e sovranità digitale sono due concetti chiave per
un sano approccio alle tecnologie informatiche da parte delle nostre
scuole e di tutta la Pubblica Amministrazione? E perché sono
altrettanto importanti affinché le aziende del territorio possano
andare a testa alta tra le cosiddette Big Tech facendo leva su di un
modello di condivisione realmente democratico basato sul Software
Libero, in grado di accelerare enormemente il processo di innovazione
tecnologica e garantire la sicurezza dei nostri dati secondo il GDPR?
Tutto ciò nel pieno rispetto del Codice dell'Amministrazione Digitale
(CAD) che pone in risalto la centralità del riuso del software e degli
investimenti dedicati alla creazione di nuovo Software Libero per
avviare l'economia nazionale in ambito IT verso un processo di massima
trasparenza e condivisione.

PROGRAMMA
---------

17:00 - **Il progetto FUSS e la creazione di valore pubblico**

- **Paolo Dongilli**, Progetto FUSS, Provincia Autonoma di Bolzano [[SLIDE]](/material/incontro-vicenza/2023-06-08_Scuole-Vicenza_Dongilli.pdf)

17:30 - **Il sistema operativo Zorin OS per una scuola libera,semplice e sicura**

- **Albano Battistella**, Zorin OS [[SLIDE]](/material/incontro-vicenza/2023-06-08_Scuole-Vicenza_Battistella.pdf)

17:45 - **Aspetti legali da considerare quando la PA acquisisce software**

- **Marco Ciurcina**, avvocato [[SLIDE]](/material/incontro-vicenza/2023-06-08_Scuole-Vicenza_Ciurcina.pdf)

18:00 - **La soluzione open source nazionale per la DDI**

- **Giorgio Favaro**, Continuity [[SLIDE]](/material/incontro-vicenza/2023-06-08_Scuole-Vicenza_Favaro.pdf)

18:30-19:00 - **Domande e discussione**

VIDEO
-----

* [Registrazione BigBlueButton](https://bbb9.comeinclasse.it/playback/presentation/2.3/bf6a2318f7876eb25d159843f06a2006e05c7b6b-1686234986938)
