---
title: "FUSS riceve il Premio PA Sostenibile"
subtitle: "Il progetto per una scuola digitalmente sostenibile premiato a Roma al Forum per la Pubblica Amministrazione."
date: 2019-05-15T21:46:13+02:00
draft: false
---

Organizzata da **ForumPA** in collaborazione con **ASviS** (Alleanza Italiana per lo Sviluppo Sostenibile), la sfida "Premio PA Sostenibile” ha lo scopo di selezionare i migliori progetti nazionali che puntano a raggiungere gli obiettivi di sviluppo sostenibile secondo quanto previsto dall'Agenda 2030 dell'ONU. Progetti e prodotti concreti in grado di aiutare il territorio nazionale ad affrontare le evidenti debolezze dell'attuale modello di sviluppo, suggerendo sentieri di crescita sostenibile da percorrere fino al 2030 ed oltre.

{{% figure src="/img/premio-fpa-convention-center.jpg" caption="Convention Center 'La Nuvola' a Roma, sede della conferenza FORUM PA 2019" %}}

<!--more-->

**7** gli **ambiti di sostenibilità** in cui i numerosi progetti si sono misurati in fase di selezione:
- Ambiente, energia, capitale naturale
- Diseguaglianze, pari opportunità, resilienza
- Economia circolare, innovazione e occupazione
- Capitale umano ed educazione
- Città, infrastrutture e capitale sociale
- Giustizia, trasparenza, partecipazione e parternariati
- Alimentazione, Salute e welfare

Il progetto [FUSS - Free Upgrade for a digitally Sustainable School](https://fuss.bz.it), nella categoria "**Capitale umano ed educazione**" è stato premiato sulla base di 4 criteri predefiniti dalla giuria: **innovatività, trasferibilità, rilevanza, sostenibilità organizzativa ed economica del progetto/prodotto**. Infatti questi sono i punti di forza di FUSS che nella sua unicità a livello nazionale ha una duplice natura: è un progetto di sostenibilità digitale per la scuola e nel contempo un prodotto free software (sistema operativo GNU/Linux ed applicativi didattici) per la gestione a 360 gradi di una rete scolastica.

FUSS è un'esperienza unica non solo in Italia ma anche in Europa per organizzazione e struttura. Il progetto ha portato alla migrazione verso software libero degli strumenti informatici utilizzati nella didattica di tutte le scuole in lingua italiana dell'Alto Adige: ad oggi lo usano 1.800 docenti e 16.000 studenti; è installato su 4.000 PC e 60 server in circa 80 scuole della Provincia Autonoma di Bolzano) alle quali si aggiungono il Liceo paritario delle scienze applicate allo sport “G. Toniolo” ed il Laboratorio di robotica educativa presso Liceo scientifico delle scienze applicate presso l’istituto paritario “Rainerum” di Bolzano, la Scuola di Musica in lingua italiana "Vivaldi", il Liceo Statale "Niccolò Rodolico" di Firenze e l’Istituto Professionale “E. Cornaro” per i Servizi di Enogastronomia e Ospitalità Alberghiera di Jesolo.  

FUSS è coordinato e finanziato dalla **Direzione Istruzione e formazione italiana della Provincia Autonoma di Bolzano** e ha permesso di rendere digitalmente sostenibile la didattica nelle scuole italiane grazie a quattro obiettivi fondamentali: l’**utilizzo di software libero**, l’**impiego di formati aperti**, la **creazione di contenuti liberi** ponendo così le basi per il quarto obiettivo il cui raggiungimento dovrebbe essere garantito da ogni scuola per definizione: **il libero accesso al sapere**.

La cerimonia di premiazione si è tenuta mercoledì **15 maggio 2019** nella cornice della conferenza  **FORUM PA 2019** organizzata al Convention Center "La Nuvola" di Roma. Il premio è stato ritirato da Paolo Dongilli, coordinatore del Progetto FUSS, a nome della Direzione Istruzione e  Formazione Italiana e di tutti i membri del Gruppo FUSS (Nucleo e Tecnici FUSS, Referenti Scolastici, Studenti e Partner Tecnologico) che ogni giorno partecipano attivamente al processo di costruzione della conoscenza e allo sviluppo del progetto stesso con l’obiettivo di **creare valore pubblico**. Infatti il denaro pubblico speso per FUSS costituisce un investimento per la Provincia Autonoma di Bolzano il cui prodotto è pubblico ed il codice sorgente unitamente alla documentazione completa sono disponibili e riusabili da tutti a norma degli **articoli 68 e 69 del Codice dell’Amministrazione Digitale**, le cui linee guida nazionali, pubblicate solo il 13 maggio scorso, vengono applicate nei loro principi dal progetto FUSS da ben 14 anni.

Link:
-----

- Progetto FUSS premiato a FORUM PA 2019: https://www.forumpachallenge.it/soluzioni/fuss-free-upgrade-digitally-sustainable-school

{{% figure src="/img/premio-fpa-poster.jpg" caption="Il poster del progetto FUSS esposto nella galleria del concorso all’interno del Convention Center" %}}

{{% figure src="/img/premio-fpa-diploma.jpg" caption="Il diploma di merito" %}}

