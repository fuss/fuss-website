---
title: "Approccio al digitale nella scuola dell'infanzia" 
subtitle: "Un progetto di digitalizzazione sostenibile del 3° Circolo didattico di Bolzano"
date: 2022-12-15T19:33:03+02:00
draft: false
---

<img src="/img/approccio-digitale-scuole-infanzia.png" alt="Percorso sui Prati del Talvera">

In risposta alle politiche internazionali che prevedono la
realizzazione di ambienti fisici e digitali di apprendimento (*on-life*)
è stato avviato un percorso di ricerca/azione che, in questa prima
fase sperimentativa, ha coinvolto le scuole dell’infanzia Città dei
bambini e Gries, entrambi presenti sul territorio della circoscrizione
Gries e facenti parte del **3° Circolo didattico di Bolzano**.

<!--more-->

L’obbiettivo è quello di intraprendere un percorso di digitalizzazione
che sia sostenibile, anche e soprattutto, in considerazione del fatto
che, essendo i bambini e le bambine i veri protagonisti del processo è
fondamentale, nel loro interesse, garantire la sovranità dei dati, per
un utilizzo del digitale che sia veramente democratico.

Per questi motivi è stata avviata nelle scuole del 3° circolo
didattico una riflessione ed una ricerca di quale possa essere la
modalità più adatta al contesto educativo che caratterizza la realtà
che ci appartiene, partendo dall’esplorazione delle risorse che già
qualificano il sistema scolastico altoatesino in ambito tecnologico,
come ad esempio le soluzioni sviluppate grazie al progetto
[**Fuss**](https://fuss.bz.it).

L’evoluzione è avvenuta attraverso la collaborazione nata con i membri
del [**Linux User Group di Bolzano (Lugbz)**](https://lugbz.org); gli
strumenti offerti (software e hardware), il prezioso accompagnamento
dei volontari del progetto [**SchoolSwap**](https://schoolswap.bz.it)
e l'ospitalità di [**ADA (Associazione per i Diritti degli
Anziani)**](http://www.adabz.it/) hanno fatto in modo che questo tipo di
tecnologia potesse effettivamente essere utilizzata in base al senso
che ha nel processo di cambiamento strutturale auspicato.

Con le realtà inerenti presenti sul territorio (Gruppo Fuss, Lugbz e
SchoolSwap), il progetto ha cercato di ipotizzare e realizzare
contesti educativi liberi e aperti dove, cosa fondamentale, a ciascun
individuo venga data la possibilità di trovare le condizioni più
adatte a conseguire la massima realizzazione di sé stesso, sviluppando
la propria identità e non ciò che altri hanno previsto per lui.

**Natascia Fattor**, docente del 3° Circolo didattico di Bolzano,
approfondisce i dettagli di questa esemplare iniziativa nel seguente
[articolo](/material/paper_approccio-digitale-scuola-infanzia_fattor.pdf),
dal quale abbiamo preso spunto per il presente resoconto.
 
[<img src="/img/paper_approccio-digitale_screenshot_border.png" alt="Approccio al digitale nella scuola dell'infanzia" width="500" border="1">](/material/paper_approccio-digitale-scuola-infanzia_fattor.pdf)

