---
title: "Sostenibilità e sovranità digitale a scuola nel pieno rispetto del CAD e del GDPR" 
subtitle: "Incontro con le scuole dell'Emilia Romagna"
date: 2023-03-28T21:53:14+02:00
draft: false
---

<img src="/img/digitale-nachhaltigkeit-icon.svg" alt="Icon Digital
Sustainabiity - Digitale Nachhaltigkeit" width="200">

Il **23 marzo 2023** si è tenuto un incontro informativo per i dirigenti
ed i docenti di circa 70 istituti dell'Emilia Romagna, su gentile
richiesta della loro DPO, ing. Enrica Marsiglio.

Considerata la situzione decisamente critica nella quale le scuole del
territorio nazionale si trovano a causa delle soluzioni in cloud
adottate nel corso degli ultimi tre anni in quanto non a norma del
GDPR, urge la necessità di informare le scuole su quali siano le
alternative viabili in grado di tutelare efficacemente i dati di
alunni e docenti. Questo il sommario ed il programma dell'incontro
(corredato da slide e video) che si è tenuto al mattino ed in replica
al pomeriggio dello stesso giorno.

<!--more-->

SOMMARIO
--------

Perché **sostenibilità** e **sovranità digitale** sono due concetti
chiave per un sano approccio alle tecnologie informatiche da parte
delle nostre scuole e di tutta la Pubblica Amministrazione? E perché
sono altrettanto importanti affinché le aziende del nostro territorio
possano andare a testa alta tra le cosiddette Big Tech facendo leva su
di un modello di condivisione realmente democratico basato sul
Software Libero, in grado di accelerare enormemente il processo di
innovazione tecnologica e garantire la sicurezza dei nostri dati
secondo il **GDPR**? Tutto ciò nel pieno rispetto del **Codice
dell'Amministrazione Digitale** che pone in risalto la centralità del
riuso del software e degli investimenti dedicati alla creazione di
nuovo Software Libero per avviare l'economia nazionale in ambito IT
verso un processo di massima trasparenza e condivisione.

PROGRAMMA
---------

**Che volto avrà la scuola del futuro? Buone leggi e buone pratiche.**

- 10:00 (15:00) Stefano Borroni Barale, docente, CUB SUR [[VIDEO ore 10:00]](https://video.fuss.bz.it/w/qj6A3B3WpyGCdAgeGViXrd)

**L'esempio del progetto FUSS**

- 10:15 (15:15)  Paolo Dongilli, Progetto FUSS, Provincia Autonoma di Bolzano [[SLIDE]](/material/incontro-er/2023-03-28_Incontro-er_dongilli.pdf)

**Cosa significa per le aziende lavorare con il modello del software libero e perché è la soluzione ideale per le scuole**

- 10:45 (15:45) Christian Surchi, Truelite
- 10:55 (15:55) Marco Marinello, QNets [[SLIDE]](/material/incontro-er/2023-03-28_Incontro-er_marinello.pdf)
- 11:05 (16:05) Giorgio Favaro, Continuity

**Aspetti legali da considerare quando la PA acquisisce software**

- 11:15 (16:15) Marco Ciurcina, avvocato [[SLIDE]](/material/incontro-er/2023-03-28_Incontro-er_ciurcina.pdf)

**Sostegno sul territorio a chilometro 0**

- 11:25 (16:25) LUG dell'Emilia Romagna (LUG Scandiano: Fausto Medici, Fabio Casolari, Denis Caleffi, Lorenzo Bondioli, Frank Giordani)

**Discussione**

- 11:35-12:00 (16:35-17:00) con la gentile collaborazione di Giacomo
Tesio.

REGISTRAZIONI
-------------

* [Registrazione BigBlueButton mattina](https://intendenzabz.comeinclasse.it/playback/presentation/2.3/6de50259cf585ab4f3bbce557093403d41043e04-1679985704179) + [Video intervento Stefano Borroni Barale](https://video.fuss.bz.it/w/qj6A3B3WpyGCdAgeGViXrd)
* [Registrazione BigBlueButton pomeriggio](https://intendenzabz.comeinclasse.it/playback/presentation/2.3/6de50259cf585ab4f3bbce557093403d41043e04-1680007279739)
