---
title: "Sempre più imprese usano Linux e software libero"
subtitle: "La cooperativa CALL di Merano sceglie FUSS"
date: 2019-12-28T23:55:22+02:00
draft: false
---

![Aula FUSS Cooperativa Call](/img/coop-call-01.jpg)

E' indiscutibile il fatto che oggigiorno ogni impresa abbia almeno un dispositivo con il sistema operativo Linux a bordo, sia esso un server, un desktop, uno smartphone o un dispositivo embedded.
Secondo i [dati riportati nel 2018](https://en.wikipedia.org/wiki/Usage_share_of_operating_systems#Market_share_by_category) (in aumento nel 2019), a livello mondiale Linux è impiegato da

- tutti i **500** più grossi **supercomputer**,
- **66,6%** dei **server web**,
- **73,19%** degli **smartphone**, 
- **29,4%** dei **sistemi embedded**.

Ciò dimostra, come già noto, che il sistema Linux è in grado di scalare ed adattarsi tanto ai piccoli dispositivi hardware quanto a quelli di grosse dimensioni, permettendo a chiunque di modificarlo a piacere, di adattarlo a nuovi sistemi e pertanto di innovare.

Anche lato desktop sale il numero di imprese, associazioni e privati che preferiscono adottare il sistema operativo Linux e software libero come ad esempio [**LibreOffice**](https://it.libreoffice.org/) per la produttività d'ufficio. Le motivazioni principali? Libertà, flessibilità, scalabilità, sicurezza, autonomia, etica, risparmio, investimenti in risorse umane, incentivazione delle aziende sul territorio, sostenibilità, economia circolare.

<!--more-->

Sul nostro territorio, tra le associazioni e cooperative che usano Linux anche in amministrazione ([**Teatro Cristallo**](https://www.teatrocristallo.it/) ed [**Arciragazzi**](http://arci.bz.it/arciragazzi/?q=node/2) per citare due esempi), si è aggiunta oggi una cooperativa che ha deciso di adottare la distribuzione Linux FUSS (basata su [Debian](https://www.debian.org)). Si tratta della [**cooperativa CALL**](https://www.coopcall.it) che si occupa di assistenza domiciliare e formazione. Nata nel 2008 a Merano da un gruppo di infermieri e assistenti professionisti, offre servizi che si espandono per tutto l'Alto Adige (Merano, Bressanone, Brunico e Vipiteno), al servizio di persone svantaggiate anche temporaneamente, autosufficienti e non, anziani, minori e disabili. 

![Logo Cooperativa Call](/img/endorsement/coop-call.png)

**FUSS**, aggiornata alla versione **10**, è stata installata sul server e su diversi PC presenti nelle sale e negli uffici della sede meranese di CALL. 

![Aula FUSS Cooperativa Call](/img/coop-call-02.jpg)

Esattamente come nelle scuole in lingua italiana, sul server fisico è stato installato l'ambiente di virtualizzazione [Proxmox VE](https://pve.proxmox.com/), pure basato su Debian, sul quale è stato creato il server FUSS virtuale per la gestione della rete informatica della cooperativa. A seguire sul server FUSS è stata depositata l'immagine dei client necessaria per installare in modo automatizzato i PC presenti in aula didattica e negli uffici grazie al sistema di installazione denominato [fuss-fucc](https://gitlab.fuss.bz.it/fuss/fucc/tree/buster) messo a punto dai tecnici del gruppo FUSS.

L'immagine predisposta per CALL comprende tutto il software necessario alla cooperativa per il proprio lavoro quotidiano, sia amministrativo che didattico. I principali applicativi sono:

- [**LibreOffice**](https://it.libreoffice.org/) per la produttività d'ufficio;
- [**Scribus**](https://www.scribus.net/) per la produzione di materiale tipografico;
- [**Dia**](http://dia-installer.de/) per la creazione di diagrammi;
- [**Asciidoctor**](https://asciidoctor.org/) per il rendering di documenti asciidoc in HTML, DocBook, PDF, ePUB;
- [**Pandoc**](https://pandoc.org/) per la conversione tra formati di markup diversi;
- [**GNU Barcode**](https://www.gnu.org/software/barcode/) per la creazione di codici a barre;
- [**Zbar**](http://zbar.sourceforge.net/) per la lettura di codicei a barre;
- [**GLabels**](https://glabels.org/) per creare etichette, biglietti da visita e copertine;
- [**PDF-Shuffler**](https://sourceforge.net/projects/pdfshuffler/) per unire, dividere e riorganizzare pagine da documenti PDF;
- [**PDFtk**](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) toolkit per la gestione di file PDF;
- [**XSane**](http://www.sane-project.org/) per l'accesso a qualsiasi dispositivo di scansione di immagini (scanner piatti, scanner manuali, videocamere, fotocamere, frame grabber, ecc.);
- [**Gimp**](https://www.gimp.org/) per l'elaborazione di immagini;
- [**Inkscape**](https://inkscape.org/it/) per il disegno vettoriale;
- [**Vokoscreen**](http://linuxecke.volkoh.de/vokoscreen/vokoscreen.html) e [**OBS Studio**](https://obsproject.com/) per la registrazione e streaming di contenuti video live;
- [**Kdenlive**](https://kdenlive.org/it/) per il montaggio di filmati e tutorial;
- [**Calibre**](https://calibre-ebook.com/) per la conversione e gestione di biblioteche di libri elettronici (e-book);
- [**Xournal**](http://xournal.sourceforge.net/) per prendere appunti (con dispositivi touch) e per editare file PDF;
- [**Openboard**](https://openboard.ch/index.it.html) per la gestione di lavagne interattive multimediali;
- [**Firefox**](https://www.mozilla.org/it/firefox/) e [**Chromium**](https://www.chromium.org/Home) per il browsing;
- [**Thunderbird**](https://www.thunderbird.net/it/) come client per la posta;
- [**Nextcloud Client**](https://nextcloud.com/clients/) per la sincronizzazione di file nel cloud privato della cooperativa;
- **pcscd**, **libccid**, **opensc**, **pcsc-tools**, **libnss3-tools** per la gestione del lettore per la Carta Provinciale dei Servizi; 
- [**Dike 6**](https://www.firma.infocert.it/installazione/) per la firma digitale e marca temporale.

e molto altro software ancora.

Tutto il software adottato sia lato server che lato client dalla cooperativa CALL è libero e riusabile da altre cooperative, associazioni e aziende, facendo leva sulle proprie risorse IT interne o richiedendo servizi di consulenza e formazione ad altre imprese del territorio che già lavorano con GNU/Linux e software libero. Alcune di queste sono elencate sul sito https://fuss.bz.it/page/profuss/.

Seguono i contatti per avere ulteriori informazioni sui servizi offerti dalla cooperativa CALL:

- **Andrea Congiu** +39 392 569 6625
- **Luigi Morganti** +39 339 495 8270
- **www**: https://www.coopcall.it
- **mail**: [info@coopcall.it](mailto:info@coopcall.it)

![Aula FUSS Cooperativa Call](/img/coop-call-03.jpg)

![Aula FUSS Cooperativa Call](/img/coop-call-04.jpg)




