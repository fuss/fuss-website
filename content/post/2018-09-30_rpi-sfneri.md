---
title: "Raspberry Pi per il registro elettronico"
subtitle: "Installate le prime dieci Raspberry Pi con FUSS"
date: 2018-09-30T14:30:23+02:00
draft: false
---

Presso la Scuola primaria "[S. Filippo Neri](http://www.spc-bz-europa1.it/sf-neri/)" di Bolzano sono state installate nelle classi le prime 10 postazioni a basso costo e consumo. 

<!--more-->

Ciascuna postazione consiste di un mini computer [Raspberry Pi 3, modello B+](https://it.wikipedia.org/wiki/Raspberry_Pi) con tastiera e mouse; grazie alle ridotte dimensioni è stato possibile fissarlo sul retro del monitor LCD da 22" in dotazione.

L'uso primario di queste postazioni è quello di permettere ai docenti di avere una postazione veloce per compilare il registro elettronico durante le lezioni, mediante un comune browser (Chromium o Firefox). Sui mini computer è installato FUSS 9 (Debian 9 "Stretch") nella versione per architettura "armhf" basata appunto su processore ARM. L'installazione è completata con applicativi di uso comune, in primis la suite d'ufficio [LibreOffice](https://it.libreoffice.org/).

Il Raspberry Pi, oltre ad essere un dispositivo a basso costo (inferiore a 40 €), presenta un consumo di energia elettrica ridottissimo: la potenza assorbita oscilla tra 1,9 e 5,1 W.

Di seguito alcune foto che mostrano le postazioni installate alla S. Filippo Neri. 

![Raspberry Pi alle S. Filippo Neri](/img/sfneri-1.jpg)

![Raspberry Pi alle S. Filippo Neri](/img/sfneri-2.jpg)

![Raspberry Pi alle S. Filippo Neri](/img/sfneri-3.jpg)

![Raspberry Pi alle S. Filippo Neri](/img/sfneri-4.jpg)
