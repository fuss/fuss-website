---
title: "Didattica & Open Source nella Scuola Pubblica" 
subtitle: "Corso di aggiornamento al Centro Studi Scuola Pubblica del Veneto"
date: 2023-10-24T22:10:14+02:00
draft: false
---

[<img src="/img/2023-10-24_cesp.png" alt="Immagine corso "Didattica e Open Source nella SCuola Pubblica">](https://cesp-cobas-veneto.eu/2023/10/05/corso-di-aggiornamento-didattica-open-source-nella-scuola-pubblica/)


Oggi si è tenuto a Padova presso il **Centro Studi per la Scuola Pubblica del Veneto** il corso di aggiornamento per il personale dirigente, docente e ATA dal titolo [**"Didattica & Open Source nella Scuola Pubblica"**](https://cesp-cobas-veneto.eu/2023/10/05/corso-di-aggiornamento-didattica-open-source-nella-scuola-pubblica/). Il corso si è svolto con la collaborazione dei progetti [**FUSS**](https://fuss.bz.it), [**Zorin**](https://zorin.com/) e [**ComeInClasse**](https://comeinclasse.it). 

[**Programma, documentazione del corso e registrazione dell'evento**](https://cesp-cobas-veneto.eu/2023/10/05/corso-di-aggiornamento-didattica-open-source-nella-scuola-pubblica/) sono disponibili sul sito del CESP Veneto.
