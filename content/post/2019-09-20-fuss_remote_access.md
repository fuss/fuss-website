---
title: "Inaugurato FUSS Remote Access"
subtitle: "Permetterà agli utenti di accedere da dovunque ai propri file"
date: 2019-09-19T19:13:17+02:00
draft: false
---

![FUSS Remote Access](/img/fuss-remote-access.png)

<h2 style="text-align:center;"><a href="https://access.fuss.bz.it">access.fuss.bz.it</a></h2>

<br>

Nell'ambito di continua innovazione da sempre proposto dal progetto FUSS,
è stato inaugurato ieri FUSS Remote Access, un nuovo ed innovativo strumento
che permetterà a docenti e studenti della provincia l'accesso
ai propri file dall'esterno della rete scolastica.
<!--more-->
Questa soluzione è basata su [Nextcloud](https://nextcloud.com) (già OwnCloud),
un software di cloud Free ed Open Source fra i più diffusi, utilizzato
anche da molte pubbliche amministrazioni Europee perché permette di salvare
i dati sui propri server piuttosto che su quelli di aziende esterne.

Partendo da alcune scuole di test, la soluzione verrà poi estesa a tutte
le scuole aderenti al progetto che ne faranno richiesta.
Per richiederne l'attivazione è sufficiente inviare una mail al Nucleo FUSS
all'indirizzo [info@fuss.bz.it](mailto:info@fuss.bz.it) a partire dal 1° novembre.
