---
title: "Si riprende l'anno scolastico con FUSS 11" 
subtitle: "Aggiornati i sistemi GNU/Linux al Cornaro di Jesolo"
date: 2022-10-22T22:28:06+02:00
draft: false
---

**a cura di Luca Marcolongo**

![Foto aula al Cornaro di Jesolo](/img/cornaro-aula-2022.jpg)
Studenti dell'istituto E. Cornaro di Jesolo.

Durante l’estate è stato fatto l’upgrade ai pc, presenti nelle aule
didattiche, ai quali sono collegati Lim e Monitor interattivi.

<!--more-->

Della nuova versione di Fuss ho apprezzato il fatto che è più veloce
nel caricare il sistema operativo e, siccome anche l'occhio vuole la
sua parte, anche il tema grafico numix adottato da FUSS ne ha reso
ancora più gradevole l’aspetto.

Qui all’Istituto Elena Cornaro, Fuss è presente nel server, che fa da
domain controller a circa 150 pc Windows (ver 10 e 11) e nei 40
notebook presenti nelle aule. Questi ultimi sono stati acquistati nel
2013 e dopo tutti questi anni grazie ad un piccolo upgrade (sono stati
sostituiti i dischi con SSD da 120 GB) sono ancora all’altezza della
situazione: con Microsoft Windows non sarebbe stato così.

Ho installato Fuss 11 in versione "standalone" su ulteriori 26
notebook per permettere a docenti ed alunni di avere un sistema
efficiente anche quando lavorano in classe o in mobilità. I notebook
sono custoditi in un carrello che da noi viene usato per lavori di
gruppo in classe. 

<img src="/img/cornaro-carrello-notebook.jpg" alt="Carrello per notebook con Fuss 11 standalone" width="400">
Carrello per la ricarica ed il trasporto dei notebook. 

Capitava spesso in passato che gli studenti lasciassero attive le
proprie sessioni dopo l'accesso agli strumenti in cloud. Grazie alla
modalità "guest" di Fuss 11 (già presente in Fuss 10 "standalone") la
privacy degli utenti viene tutelata in quanto all'uscita dalla propria
sessione il profilo utente viene cancellato e ricreato all'ingresso
dell'utente successivo.

![Fuss 11 standalone](/img/cornaro-notebook-fuss-11-standalone.jpg)
Fuss 11 "standalone" installata su 26 notebook.

Per quanto riguarda i docenti, apprezzano il fatto che il loro
ambiente di lavoro (Scrivania, Preferiti, password...) li accompagni
in tutte le aule semplificando gli aspetti organizzativi, la gestione
dei file e le formalità del registro elettronico.

Per quanto riguarda me, sono l’unico tecnico, il lavoro è tanto e
seguire i vari sistemi a volte impegna anche fuori l’orario di
servizio, però per il momento la soddisfazione di contribuire a far
conoscere la sostenibilità digitale nel mio istituto mi ripaga.

Ringrazio il gruppo Fuss e l’Intendenza Scolastica italiana di Bolzano
perché il progetto è vivo e attivo; soprattutto ringrazio Paolo
Dongilli che mi ha adottato e mi rende partecipe delle novità dandomi
molto spesso dei suggerimenti tecnici.  All'inizio dell'anno
scolastico tutto deve funzionare a puntino, anche e soprattutto le
attrezzature informatiche in un periodo in cui "didattica a distanza"
(o DaD) e "didattica digitale integrata" sono entrate nel vocabolario
della scuola.
