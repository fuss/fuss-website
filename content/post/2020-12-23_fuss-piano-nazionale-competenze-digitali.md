---
title: "Strategia Nazionale per le competenze digitali" 
subtitle: "Sostenibilità digitale e Progetto FUSS presenti nel Piano Operativo"
date: 2020-12-23T22:35:17+02:00
draft: false
---

Dopo la pubblicazione del Decreto a luglio scorso, oggi, 23 dicembre 2020 è stato ufficializzato sul sito di Repubblica Digitale il [**Piano Operativo della Strategia Nazionale per le Competenze Digitali**](https://repubblicadigitale.innovazione.gov.it/pubblicato-piano-operativo-strategia-nazionale-competenze-digitali/).

All'interno del [**Piano Operativo**](https://repubblicadigitale.innovazione.gov.it/assets/docs/Piano-Operativo-Strategia-Nazionale-per-le-competenze-digitali.pdf) è prevista un'iniziativa specifica sulla **Sostenibilità Digitale** (Azione 20) riguardante lo *Sviluppo di competenze e cultura digitale degli studenti*. Il **Progetto FUSS**, quale principale progetto di sostenibilità digitale nelle scuole della Provincia Autonoma di Bolzano, è orgoglioso di essere annoverato tra le **36** iniziative della **Coalizione nazionale** destinate al mondo dell'istruzione contribuendo al raggiungimento degli obiettivi dell'Asse 1 (Competenze digitali nel ciclo dell'istruzione e della formazione superiore).

<!--more-->
