---
title: About FUSS
subtitle: Information material
---

A brochure of the project is available in two bilingual variants:

- [German-Italian FUSS Brochure](/material/brochure-fuss-de-it-v6.pdf)
- [English-Italian FUSS Brochure](/material/brochure-fuss-en-it-v6.pdf)

![brochure photo](/img/foto-volantino.jpg)
