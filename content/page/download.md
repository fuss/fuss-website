---
title: Download...
subtitle: Download FUSS server and client and create an educational network
---

## Server

To install a FUSS server (whether physical or virtual), simply use the official Debian 12 "bookworm" image, following the instructions given in the [FUSS Tech Guide](https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#installazione-di-fuss-server-tradizionale).

If the FUSS server is installed as a virtual machine in the Proxmox VE virtualization environment, it is recommended to follow the quick installation procedure via cloud-init presented in the [FUSS Tech Guide](https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html#installazione-di-fuss-server-dall-immagine-cloud-init).

## Client/Desktop/Live

FUSS PC .iso images can be copied to a USB stick or DVD and used in three different ways: 

1. **live**, without the need to install anything;
2. **desktop** "standalone," installing the distribution on a satellite PC not tied to a specific educational network;
3. **client** for connection to a server in an educational network.

[FUSS 12 images](http://iso.fuss.bz.it/fuss12/client/) are available in *base* and *education* versions. The latter incorporates all educational software for schools of all grades.

### Archive of previous releases

Previous releases are available at the following address: [http://iso.fuss.bz.it](http://iso.fuss.bz.it).
