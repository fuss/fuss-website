---
title: About FUSS
subtitle: Brief History of the FUSS Project
---

The **project FUSS** (Free Upgrade in South Tyrol's Schools) started in 2005 with the aim of bringing [Free Software](https://it.wikipedia.org/wiki/Software_libero) to schools in the Autonomous Province of Bolzano. The project was financed by the European Social Fund and administratively managed by the [Scuola Professionale Einaudi di Bolzano](http://www.cts-einaudi.fpbz.it), which chose to take advantage of the advice and collaboration of an Italian company operating in the Free Software IT market, the company [Truelite Srl](https://www.truelite.it/). From 2006 to 2015 the project continued with teaching staff from the Italian Training and Education Department adequately trained to support teachers in teaching and manage the fleet of machines (about 4,000 desktops and 65 servers) in the schools.
Since 2015, the IT Department of the Province of Bolzano has taken over the technical support of the Italian-language schools with a group of GNU/Linux system administrators (**FUSS technicians**) on the basis of a [formal agreement](/material/Accordo-FUSS-2016-vigente.pdf) entered into with the Italian School Board in 2016.

The governance of the project is in the hands of the [Italian Education and Training Directorate](http://www.forumpachallenge.it/proponenti/direzione-istruzione-e-formazione-italiana-provincia-autonoma-di-bolzano) of the Autonomous Province of Bolzano with the **FUSS** Team Core composed of Paolo Dongilli, Claudio Cavalli and Andrea Bonani, the current technology partner Marco Marinello, and with the collaboration of the inspector for the mathematical, scientific and technological field Fabio Furciniti.

At each school there is a **technical referee**; in total there are about 70 teachers who serve as a point of contact to FUSS technicians, for support and maintenance requests, and to the FUSS Team Core, for requests related to installed software, requests for new software or training requests.

FUSS Team Core, Technicians and Referents together make up the **FUSS Group** whose purpose is to develop, maintain and disseminate a cutting-edge project on the national territory that has made it possible to make ICT (information and communication technologies) teaching digitally sustainable in Italian-language schools thanks to four fundamental objectives: 

* the use of Free Software, 
* the use of open formats, and 
* the creation of free content 

thus laying the foundation for the fourth goal whose achievement should be guaranteed by every school by definition: **free access to knowledge**.

## The teaching model

The choice to use free software in schools is first and foremost, beyond economic or technical reasons, an ethical and political choice. That is, it is the choice to refer, in teaching, to the values of freedom and knowledge sharing, and not only to use efficient, stable and secure software.

The philosophy behind free software, that of freedom of access to information and knowledge sharing, naturally fits the educational task of a new school.

Moreover, using free software represents a choice to use common heritage of humanity, the improvement and dissemination of which benefits everyone, not a single entity.

The use of free software makes it possible for students and teachers to participate directly in its development, not only as code writing, but especially in terms of suggestions on operation, production of documentation, translations, content realizations, etc. 
All this in a model of a school seen as a community in which all its components actively participate in the process of knowledge construction.

The medium- to long-term goal of this project is to foster collaborative teaching methodologies through the active involvement of both teachers and students in the development of the project itself.
