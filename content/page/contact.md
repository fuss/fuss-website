---
title: Contacts
#subtitle: 
---

If you have any questions about FUSS or would like to collaborate, please send an email to <info@fuss.bz.it>.

Subscribe to our public mailing lists:

- [fuss-community](https://postorius.fuss.bz.it/postorius/lists/fuss-community.lists.fuss.bz.it/): General discussions regarding FUSS.
- [fuss-devel](https://postorius.fuss.bz.it/postorius/lists/fuss-devel.lists.fuss.bz.it/): Coordinating software development activities.

Mailing list [Archives](https://hyperkitty.fuss.bz.it) are also available. 
