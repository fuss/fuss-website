---
title: Sportello
#subtitle: 
---

E' attivo a **Bolzano**, **Merano** e **Brunico** lo [**Sportello Open &
Linux**](https://www.fuss.bz.it/open-linux-desk). 
Un gruppo di volontari è a disposizione dei cittadini gratuitamente
due volte al mese per

- installare e configurare il sistema operativo GNU/Linux ed in
particolare le distribuzioni FUSS e Linux Mint;
- installare e configurare software libero (p.es. LibreOffice, Firefox,
Gimp, ...);
- guidare all'uso di GNU/Linux e di software libero come avviene nelle
scuole grazie al progetto [FUSS](https://fuss.bz.it);
- configurare il lettore di smartcard per accedere ai servizi di
eGovernment;
- fornire aiuto per la richiesta delle credenziali SPID (Sistema
Pubblico di Identità Digitale).

E' richiesta 
l'[**ISCRIZIONE**](https://www.fuss.bz.it/open-linux-desk/it/pages/01-iscrizione.html)
on-line per i soli sportelli di Bolzano e Merano, mentre per Brunico al momento l'accesso è libero senza prenotazione.

## Partner

L'avvio dello Sportello è possibile grazie al supporto dei seguenti enti ed associazioni:

* [**Quartiere
Europa-Novacella**](http://www.comune.bolzano.it/quartieri_context.jsp?area=107&ID_LINK=975) della Città di Bolzano
* [**Realgymnasium "Albert Einstein"**](https://www.rg-me.it/) e [**Technische Fachoberschule "Oskar von Miller"**](http://www.tfo-meran.it/) di Merano
* [**Librika, biblioteca civica di Brunico**](https://biblio.bz.it/brunico/)
* [**Intendenza Scolastica Italiana**](http://www.provincia.bz.it/intendenza-scolastica), Provincia Autonoma di Bolzano
* Gruppo [**Digital Sustainability Südtirol-Alto Adige**](https://openbz.eu?lang=it)
* Associazione [**LUGBZ**](https://www.lugbz.org) (Linux User Group Bozen-Bolzano-Bulsan)
* Associazione [**LinuxTrent**](https://www.linuxtrent.it)

ed ai numerosi informatici volontari che si sono resi disponibili a fornire il loro servizio gratuito alla cittadinanza.

![Loghi degli enti ed associazioni che patrocinano o sportello](/img/sponsor-open-linux-desk.png)
