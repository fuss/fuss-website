---
title: FUSS Community
subtitle: Join our network now! 
---

This page is dedicated to schools, educational institutions,
organizations and environments/areas where **FUSS** is *used*,
*studied*, *shared* and *improved*.

If you want to be part of our community, don't hesitate to [**contact
us**](https://fuss.bz.it/page/contact/)! We will help you move away
from proprietary software lock-ins towards a sustainable
digitalization process based on Free Software.

Below is the list of members in the FUSS Community. If you are not
listed, please let us know.

- All primary and secondary public schools in Italian language of the Autonomous Province of Bozen-Bolzano (11 *istituti comprensivi*, 4 *istituti pluricomprensivi*, 9 secondary schools):
      - Istituto comprensivo Bassa Atesina
      - Istituto comprensivo Bolzano I – Centro storico
      - Istituto comprensivo Bolzano II – Don Bosco
      - Istituto comprensivo Bolzano III – Viale Trieste
      - Istituto comprensivo Bolzano IV – Oltrisarco
      - Istituto comprensivo Bolzano V – Gries 1
      - Istituto comprensivo Bolzano VI – Via Rovigo
      - Istituto comprensivo Bolzano – Europa 2
      - Istituto comprensivo Laives I
      - Istituto comprensivo Merano I
      - Istituto comprensivo Merano II
      - Istituto pluricomprensivo Bolzano-Europa 1
      - Istituto pluricomprensivo Bressanone
      - Istituto pluricomprensivo Brunico – Val Pusteria
      - Istituto pluricomprensivo Vipiteno – Alta Val d´Isarco
      - Istituto di istruzione secondaria di II grado "Galileo Galilei" - Bolzano
      - Istituto di istruzione secondaria di II grado "Gandhi" - Merano
      - Istituto di istruzione secondaria di II grado - Bressanone
      - Istituto di istruzione secondaria di II grado per le scienze umane, i servizi e il turismo "C. de' Medici" - Bolzano
      - Istituto Tecnico Economico "Cesare Battisti" - Bolzano
      - Istituto tecnico per le costruzioni, l'ambiente e il territorio "A. e P. Delai" - Bolzano
      - Liceo "Evangelista Torricelli" - Bolzano
      - Liceo "Giosuè Carducci" - Bolzano
      - Liceo "Giovanni Pascoli" - Bolzano
- III Circolo delle scuola dell'infanzia di Bolzano
- Liceo paritario delle scienze applicate allo sport “G. Toniolo”;
- Scuola di Musica in lingua italiana "Vivaldi";
- Liceo Statale "Niccolò Rodolico" di Firenze;
- Istituto Professionale “E. Cornaro” per i Servizi di Enogastronomia e Ospitalità Alberghiera di Jesolo;
- Liceo Scientifico "Savoia Benincasa di Ancona;
- IISS "Bruno Munari" di Castelmassa (Rovigo).

FUSS was also adopted by some of [**Bolzano's
preschools**](https://fuss.bz.it/post/2019-10-17_scuole-infanzia/)
whose computer equipment, often dated, mounted insecure versions of
proprietary operating systems. Several requests came to the FUSS Team
in which directors emphasized the need for up-to-date, usable systems
for teachers.

FUSS is used as a reference distribution by the
[**SchoolSwap**](https://schoolswap.it/wp/?page_id=24&lang=de) project
in the Province of Bolzano; this digital sustainability and solidarity
project aims to recover PCs from firms or public agencies to give them
for free to students and families who need a computer to study or
work.

During the period of the COVID-19 pandemic, the need to recover used
desktop PCs and laptops to provide to workers in the public and
private sectors for [**smart
working**](https://fuss.bz.it/post/2020-04-17_fuss-in-amministrazione/),
allowed the further dissemination of FUSS as a lightweight operating
system suitable for the reuse of computers otherwise no longer usable
with proprietary operating systems.

![PMPC](/img/community/pmpc.png)

![FSFE Infographic 1](/img/community/fsfe-infographic-01.png)

![FSFE Infographic 2](/img/community/fsfe-infographic-02.png)
