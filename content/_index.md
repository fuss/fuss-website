FUSS is a complete GNU/Linux solution (server, client and desktop/standalone) based on [Debian](https://www.debian.org) for managing an educational network.

It is at the same time also a [digital sustainability](https://openbz.eu/?lang=it) project that since 2005 allows students and teachers to use at home the same IT tools installed at school, freely and at no cost.
